package com.suim.resource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ErrorController {
	
	public String errors(Model model,HttpServletRequest request, HttpServletResponse response){
		int errorCode = response.getStatus();
		model.addAttribute("msg",generateMessage(errorCode));
		return "/error/error";
	}
	
	private String generateMessage(int code){
		switch(code){
			case 404 : 
				return "404 Not Found";
			case 500 : 
				return "500 Internal Server Error";
			case 403 : 
				return "403 Forbidden Access";
				
		}
		return "";
	}
	
}
