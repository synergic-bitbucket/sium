package com.suim.resource;

import java.util.List;

public interface DAOInterface<T>{
	public T getById(int id);	
	public List<T> getByQuery(String query);	
	public List<T> getAll(int start,int limit);		
	public List<T> getAll();			
	public boolean update(int id,T data);
	public boolean delete(int id);
	public boolean create(T data);
}