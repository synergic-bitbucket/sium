package com.suim.resource.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus( value = HttpStatus.BAD_REQUEST )
public class BadRequest extends RuntimeException {

	public BadRequest() {
		// TODO Auto-generated constructor stub
	}

}
