package com.suim.resource.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus( value = HttpStatus.INTERNAL_SERVER_ERROR )
public class ErrorException extends RuntimeException{

	public ErrorException() {
		// TODO Auto-generated constructor stub
	}

}
