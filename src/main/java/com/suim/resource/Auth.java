package com.suim.resource;

import com.suim.resource.error.Unauthorized;
import com.suim.user.Personil;
import io.jsonwebtoken.*;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;

public class Auth {

    private static final String API_KEY = "su1mApPs";
    public static final String ISSUER = "suim-app";
    public static final String LOGIN_SUBJECT = "login-user";
    public static final int EXPIRED_TIME = 1000 * 60 * 24 * 30 * 12;

    public Auth() {
    }

    public static String createJWT(String id, String issuer, String subject, long ttlMillis, Personil user) {

        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(API_KEY);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        JwtBuilder builder = Jwts.builder().setId(id)
                .setIssuedAt(now)
                .setSubject(subject)
                .setIssuer(issuer)
                .signWith(signatureAlgorithm, signingKey);

        if (ttlMillis >= 0) {
            long expMillis = nowMillis + ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }

        //Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }

    public static Jws<Claims> getData(String tokenSession) {
        try {
            return Jwts.parser()
                    .setSigningKey(DatatypeConverter.parseBase64Binary(API_KEY))
                    .parseClaimsJws(tokenSession);
        } catch (Exception e) {
            throw new Unauthorized();
        }
    }

    public static Jws<Claims> getTokenData(String tokenSession) {
        return Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(API_KEY))
                .parseClaimsJws(tokenSession);
    }

    public static String getId(String tokenSession) {
        return getData(tokenSession).getBody().getId();
    }

}
