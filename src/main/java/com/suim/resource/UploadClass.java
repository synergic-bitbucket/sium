package com.suim.resource;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.web.multipart.MultipartFile;

public class UploadClass {
	
	private static final String UPLOAD_PAtH = "/upload/";
	
	public static String uploadClass(MultipartFile file,String newName){
		try {
			Path path = Paths.get(UPLOAD_PAtH+newName);
			File uploadDir = new File(UPLOAD_PAtH);
			
			if( !uploadDir.exists() ){
				uploadDir.mkdirs();
			}
			
			InputStream inputStream = file.getInputStream();
			OutputStream outputStream = Files.newOutputStream(path);
			IOUtils.copy(inputStream, outputStream);
			return file.getOriginalFilename();			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}
