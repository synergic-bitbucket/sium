package com.suim.disposisi;

import java.io.FileInputStream;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.jayway.jsonpath.internal.Path;
import com.suim.resource.UploadClass;
import com.suim.surat.Surat;
import com.suim.surat.SuratController;
import com.suim.surat.SuratDAO;
import com.suim.user.Personil;

@Controller
@RequestMapping("/disposisiSurat")
public class DisposisiController {
	
	@Autowired
	SuratDAO suratDAO;
	
	@RequestMapping( value="/create", method=RequestMethod.GET )
	public String disposisiSurat(@RequestParam(defaultValue = "0", required=true) int id,Model model){
		Surat surat = suratDAO.getById(id);		
		model.addAttribute("suratDetails", surat);
		model.addAttribute("teruskan_ka_code", SuratDAO.TERUSKAN_KA);		
		model.addAttribute("isSripim", Personil.getRoles().equals(Personil.USER_SRIPIM));		
		model.addAttribute("isWaka", Personil.getRoles().equals(Personil.USER_WAKA));				
		
		return "surat/disposisi_create";
	}
	
	@RequestMapping( value="/create", method=RequestMethod.POST )
	public String saveDisposisi(Surat surat,@RequestParam( required = false) MultipartFile file_disposisi_upload,Model model){
		if( file_disposisi_upload != null && file_disposisi_upload.getSize() > 0 ){
			String name = UploadClass.uploadClass(file_disposisi_upload, file_disposisi_upload.getOriginalFilename());
			surat.setFile_disposisi(name);
		}
		System.out.println(suratDAO.updateDisposisi(surat)+"affected");
		return "redirect:/disposisi";
	}
	
	
}
