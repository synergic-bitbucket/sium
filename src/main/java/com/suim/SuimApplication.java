package com.suim;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class SuimApplication{// extends SpringBootServletInitializer {

    public static int online = 0;

	public static void main(String[] args) {
//        new SpringApplicationBuilder(SuimApplication.class).application().run(args);
		SpringApplication.run(SuimApplication.class, args);
	}

//    @Override
//    protected SpringApplicationBuilder configure(SpringApplicationBuilder applicationBuilder) {
//        return applicationBuilder.sources(application);
//    }
//
//    private static Class<SuimApplication> application = SuimApplication.class;
}
