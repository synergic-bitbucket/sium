package com.suim.api.response;

import com.suim.user.Personil;

public class LoginResponse{
	Personil personil;
	String message;
	String token;
	
	public LoginResponse() {
		// TODO Auto-generated method stub
	}
	
	public LoginResponse(Personil personil,String message,String token) {
		this.personil = personil;
		this.message = message;
		this.token = token;
	}
	
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Personil getPersonil() {
		return personil;
	}	
	
	public void setPersonil(Personil personil) {
		this.personil = personil;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
