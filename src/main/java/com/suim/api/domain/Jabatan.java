package com.suim.api.domain;

import io.jsonwebtoken.lang.Collections;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by raizal.pregnanta on 07/11/2016.
 */
public class Jabatan {
    private long id_jabatan;
    private String jabatan;
    private List<String> tokens;

    public Jabatan() {
    }

    public Jabatan(long id_jabatan, String jabatan) {
        this.id_jabatan = id_jabatan;
        this.jabatan = jabatan;
        tokens = new ArrayList<>(Collections.arrayToList(jabatan.split(" ")));
    }

    public long getId_jabatan() {
        return id_jabatan;
    }

    public void setId_jabatan(long id_jabatan) {
        this.id_jabatan = id_jabatan;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public List<String> getTokens() {
        return tokens;
    }

    public void setTokens(List<String> tokens) {
        this.tokens = tokens;
    }
}
