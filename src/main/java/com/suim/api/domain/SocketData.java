package com.suim.api.domain;

/**
 * Created by raizal.pregnanta on 09/11/2016.
 */
public class SocketData {

    public static final String TYPE_SURAT_MASUK="surat-masuk";
    public static final String TYPE_DISPOSISI="disposisi";

    private String type;
    private Object object;

    public SocketData() {
    }

    public SocketData(String type, Object object) {
        this.type = type;
        this.object = object;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
}
