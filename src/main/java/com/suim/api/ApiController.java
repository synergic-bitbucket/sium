package com.suim.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.suim.api.domain.Jabatan;
import com.suim.api.domain.SocketData;
import com.suim.api.response.LoginResponse;
import com.suim.config.WebSocketHandler;
import com.suim.notification.NotificationHandler;
import com.suim.resource.Auth;
import com.suim.resource.error.BadRequest;
import com.suim.surat.Surat;
import com.suim.surat.SuratDAO;
import com.suim.surat.SuratMasukHandler;
import com.suim.surat.category.SuratCategory;
import com.suim.surat.category.SuratCategoryDAO;
import com.suim.user.Personil;
import com.suim.user.PersonilDAO;
import com.suim.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class ApiController {

    @Autowired
    public SimpMessageSendingOperations messagingTemplate;
    @Autowired
    PersonilDAO personilDAO;
    @Autowired
    SuratDAO suratDAO;
    @Autowired
    SuratCategoryDAO suratCategoryDAO;

    @Autowired
    WebSocketHandler webSocketHandler;

    @Autowired
    private SimpMessagingTemplate brokerMessagingTemplate;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public
    @ResponseBody
    LoginResponse login(String username, String password) {

        Personil personil = null;
        User user = personilDAO.userByUsername(username);
        try {
            System.out.println(new ObjectMapper().writeValueAsString(user));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        String message = null;
        String token = null;

        System.out.println("username : " + username);
        System.out.println("password : " + password);

        try {
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            if(encoder.matches(password,user.getPassword()) && !user.getRole().equals(Personil.USER_ADMIN)){
                personil = user.getPersonil();
                message = "Login berhasil";
                token = Auth.createJWT(String.valueOf(personil.getId_personil()), Auth.ISSUER, Auth.LOGIN_SUBJECT, Auth.EXPIRED_TIME, personil);
            }
        } catch (Exception e) {
            message = "Login Gagal, Username / password salah";
        }
        return new LoginResponse(personil, message, token);
    }

    @RequestMapping(value = "/getSuratDisposisi", method = RequestMethod.POST)
    public
    @ResponseBody
    List<Surat> getSuratDisposisi(
            String token,
            @RequestParam(required = false, defaultValue = "all")
                    String type,
            @RequestParam(required = false, defaultValue = "1")
                    int page,
            @RequestParam(required = false, defaultValue = "10")
                    int count
    ) {

        Personil userDetail = getPersonilDetail(token);

        String query = suratDAO.getSuratDisposisiByRole(String.valueOf(userDetail.getId_jabatan()), type, page, count);
        System.out.println(query);
        List<Surat> result = suratDAO.getByQuery(query, new SuratDAO.SuratFullMapper());

        for (Surat surat : result) {
            surat.setPenerima(suratCategoryDAO.getDistribusiSurat(surat.getId_surat()));
        }

        return result;
    }


    @RequestMapping(value = "/getSurat", method = RequestMethod.POST)
    public
    @ResponseBody
    List<Surat> getSurat(
            String token,
            @RequestParam(required = false, defaultValue = "all")
                    String type,
            @RequestParam(required = false, defaultValue = "1")
                    int page,
            @RequestParam(required = false, defaultValue = "10")
                    int count
    ) {
        Personil userDetail = getPersonilDetail(token);

        switch (userDetail.getId_jabatan() + "") {
            case Personil.USER_ADMIN:
            case Personil.USER_KA:
            case Personil.USER_SRIPIM:
            case Personil.USER_WAKA:
                SuratMasukHandler suratMasukHandler = suratDAO.getSuratByRole(String.valueOf(userDetail.getId_jabatan()), type,"", page, count, "datetime_diterima","desc");
                System.out.println(suratMasukHandler.getQuery());
                return suratDAO.getByQuery(suratMasukHandler.getQuery(), suratMasukHandler.getRowmapper());
            default:
                throw new BadRequest();
        }

    }

    @RequestMapping(value = "/statussurat", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    HashMap statusSurat(String token) {

        Personil personil = getPersonilDetail(token);

        HashMap data = new HashMap<>();

        HashMap dataCategory = new HashMap();
        dataCategory.put("all", suratDAO.countSuratMasuk("all", personil.getId_jabatan() + ""));
        for (SuratCategory category : suratCategoryDAO.getAll()) {
            dataCategory.put(category.getKode_surat(), suratDAO.countSuratMasuk(category.getKode_surat(), personil.getId_jabatan() + ""));
        }
        data.put("surat_masuk", dataCategory);

        dataCategory = new HashMap();
        dataCategory.put("all", suratDAO.countDisposisi("all", personil.getId_jabatan() + ""));
        for (SuratCategory category : suratCategoryDAO.getAll()) {
            dataCategory.put(category.getKode_surat(), suratDAO.countDisposisi(category.getKode_surat(), personil.getId_jabatan() + ""));
        }
        data.put("disposisi", dataCategory);

        return data;
    }

    @RequestMapping(value = "/teruskanKA", method = RequestMethod.POST)
    public
    @ResponseBody
    String fowardKA(String token, int id_surat) {

        try {
            Personil userDetail = getPersonilDetail(token);
            Surat surat = suratDAO.getById(id_surat);
            surat.setStep_surat(SuratDAO.TERUSKAN_KA);
            if (suratDAO.update(id_surat, surat)) {
                this.sendMessage(String.valueOf(userDetail.getId_jabatan()), false);
                return "Surat Berhasil Di Teruskan ke KA";
            }
        } catch (Exception e) {

        }
        return "Surat Gagal Terukan ke KA";
    }

    @RequestMapping(value = "/disposisi", method = RequestMethod.POST)
    public
    @ResponseBody
    Map disposisi(String token, int id_surat, String disposisi) {

        Map result = new HashMap();
        result.put("status",false);
        result.put("message","Gagal melakukan disposisi");
        try {
            Personil userDetail = getPersonilDetail(token);

            switch (userDetail.getId_jabatan() + "") {
                case Personil.USER_KA:
                case Personil.USER_WAKA:

                    Surat surat = suratDAO.getById(id_surat);
                    surat.setDisposisi(disposisi);
                    if (suratDAO.updateDisposisi(surat, userDetail.getNrp() + ""
                            , userDetail.getId_jabatan() + "")
                            ) {

                        String json = null;
                        try {
                            json = new ObjectMapper().writeValueAsString(suratDAO.getById(id_surat));
                            messagingTemplate.convertAndSend("/disposisi/jabatan/" + Personil.USER_SRIPIM, json);
                        } catch (JsonProcessingException e) {
                            e.printStackTrace();
                        }
                        result.put("status",true);
                        result.put("message","Berhasil melakukan disposisi");
                    }
                    break;
                default:
                    throw new BadRequest();
            }

        } catch (Exception e) {

        }
        return result;
    }

    @RequestMapping(value = "/jenissurat", method = RequestMethod.GET)
    public
    @ResponseBody
    HashMap<String, Object> getJenisSuratMasuk() {

        HashMap data = new HashMap<>();

        try {
            data.put("surat_masuk", suratCategoryDAO.getAll());
            data.put("surat_keluar", suratCategoryDAO.getAllSuratKeluarCategories());
        } catch (Exception e) {

        }
        return data;
    }

    @RequestMapping(value = "/teruskan", method = RequestMethod.POST)
    @ResponseBody
    public Map teruskanKeKA(@RequestParam(required = true) int id) {
        Map result = new HashMap();
        result.put("status",true);
        suratDAO.teruskan(id);
        String json = null;
        try {
            json = new ObjectMapper().writeValueAsString(suratDAO.getById(id));
            messagingTemplate.convertAndSend("/surat-masuk/jabatan/"+Personil.USER_SRIPIM,json);
            messagingTemplate.convertAndSend("/surat-masuk/jabatan/"+Personil.USER_KA,json);
            webSocketHandler.sendToJabatan(Personil.USER_KA,new SocketData(SocketData.TYPE_SURAT_MASUK,json));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            result.put("status",false);
        }

        return result;
    }

    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    @ResponseBody
    public HashMap<String, Object> uploadFile(
            @RequestParam("uploadfile") MultipartFile uploadfile) {

        HashMap<String, Object> respond = new HashMap();

        try {
            // Get the filename and build the local file path (be sure that the
            // application have write permissions on such directory)
            String filename = uploadfile.getOriginalFilename().replaceAll(" ", "_");
            String directory = "upload/";

            File dir = new File(directory);
            if (!dir.exists())
                dir.mkdirs();

            String filepath = Paths.get(directory, filename).toString();

            File file = new File(filepath);
            if (file.exists()) {
                String[] fileName = filename.split("\\.(?=[^\\.]+$)");

                int i = 1;
                while (new File(Paths.get(directory, fileName[0] + "(" + (i) + ")." + fileName[1]).toString()).exists()) {
                    i++;
                }
                filename = fileName[0] + "(" + (i) + ")." + fileName[1];
                filepath = Paths.get(directory, filename).toString();
            }

            // Save the file locally
            BufferedOutputStream stream =
                    new BufferedOutputStream(new FileOutputStream(new File(filepath)));
            stream.write(uploadfile.getBytes());
            stream.close();
            respond.put("status", true);
            respond.put("code", 200);
            respond.put("message", "Upload sukses");
            respond.put("path", "/doc/" + filename);
        } catch (Exception e) {
            respond.put("status", false);
            respond.put("code", 500);
            respond.put("message", e.getMessage());
            e.printStackTrace();
        }
        return respond;
    } // method uploadFile

    private Personil getPersonilDetail(String token) {
        System.out.println("ID : "+Integer.parseInt(Auth.getId(token)));
        return personilDAO.getById(Integer.parseInt(Auth.getId(token)));
    }

    @RequestMapping(value = "/detailSurat", method = RequestMethod.POST)
    public Surat surat(String token, int id) {
        getPersonilDetail(token);
        return suratDAO.getById(id);
    }

    @RequestMapping(value = "/jabatan", method = RequestMethod.GET)
    public List<Jabatan> jabatan() {
        return suratCategoryDAO.getAllJabatan();
    }

    private void sendMessage(String idJabatan, boolean forSripim) {
        try {
            NotificationHandler.ResponseStep responseStep = NotificationHandler.getStep(
                    String.valueOf(idJabatan), forSripim);
            for (String jabatan : responseStep.jabatan) {
                this.brokerMessagingTemplate.convertAndSend(NotificationHandler.PREFIX_MESSAGE + jabatan + NotificationHandler.MESSAGE_DISPOSISI, responseStep.msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}