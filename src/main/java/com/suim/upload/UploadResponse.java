package com.suim.upload;

public class UploadResponse {
	String msg;
	int status;
	String fileName;
	
	public UploadResponse(String msg,int status,String fileName) {
		this.msg = msg;
		this.status = status;
		this.fileName = fileName;
	}
	
	public UploadResponse() {
		// TODO Auto-generated constructor stub
	}
	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
