package com.suim.upload;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class Upload {
	private final String path  = "/src/main/resources/static/document/";
	
	@RequestMapping( value= "/upload",method = RequestMethod.POST )
	public @ResponseBody UploadResponse upload(String fileUploadedBefore,@RequestParam("file") MultipartFile file){
		File uploadedFile = new File(path+fileUploadedBefore);		
		UploadResponse response = new UploadResponse();
		try{
			
			if( this.getFileExtension( file.getOriginalFilename() ).equals("pdf")  ){			
				InputStream inputStream = file.getInputStream();
				Path path = Paths.get(this.path+file.getOriginalFilename());
				OutputStream outputStream = Files.newOutputStream(path);				
				IOUtils.copy(inputStream, outputStream);
				
				if( uploadedFile.exists() ){
					uploadedFile.delete();
				}
				
				response.setStatus(200);
				response.setMsg("Upload File Berhasil");
				response.setFileName(file.getOriginalFilename());
			}
		}catch( Exception e ){
			e.printStackTrace();
		}
		response.setStatus(500);
		response.setMsg("Upload File Gagal");
		
		return response;
	}
	
	
	private String getFileExtension(String name){
		String[] fileData = name.split(".") ;
		return fileData.length > 0 ? fileData[ fileData.length - 1 ] : null;
	}
}
