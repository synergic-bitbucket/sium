package com.suim.login;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {
	
	@RequestMapping("/login")
	public String loginAct(){
		return "login/login";
	}
	
	@RequestMapping("/logout")
	public String logout(){
		return "";
	}
}
