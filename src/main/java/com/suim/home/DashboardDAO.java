package com.suim.home;

import com.suim.SuimApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by raizal.pregnanta on 22/12/2016.
 */
@Service("DashboardDAO")
public class DashboardDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public int currentOnline(){
        SuimApplication.online = SuimApplication.online>0?SuimApplication.online:0;
        return SuimApplication.online;
    }

    public int todayDisposisi(){
        return jdbcTemplate.queryForObject("select count(*) from surat_masuk where step_surat in (3,5,6,7) and date(waktu_disposisi)=date(NOW())",Integer.class);
    }

    public int todaySuratKeluar(){
        return jdbcTemplate.queryForObject("select count(*) from surat_keluar where date(time_created)=date(NOW())",Integer.class);
    }

    public int todaySuratMasuk(){
        return jdbcTemplate.queryForObject("select count(*) from surat_masuk where date(waktu_disposisi)=date(NOW())",Integer.class);
    }

    ResultSetExtractor<LinkedHashMap> extractor = new ResultSetExtractor<LinkedHashMap>() {
        @Override
        public LinkedHashMap extractData(ResultSet resultSet) throws SQLException, DataAccessException {
            LinkedHashMap data = new LinkedHashMap();
            if(resultSet.first()){
                do{
                    data.put(resultSet.getString(1),resultSet.getInt(2));
                }while(resultSet.next());
            }
            return data;
        }
    };

    public LinkedHashMap monthlyDisposisi(){
        String sql = "SELECT date(datetime_diterima) as 'date',count(*) as 'count' from surat_masuk where date(datetime_diterima) BETWEEN\n" +
                "DATE_SUB(\n" +
                "    LAST_DAY(\n" +
                "        NOW()\n" +
                "    ), \n" +
                "    INTERVAL DAY(\n" +
                "        LAST_DAY(\n" +
                "            NOW()\n" +
                "        )\n" +
                "    )-1 DAY\n" +
                ")\n" +
                "and \n" +
                "LAST_DAY(\n" +
                "    DATE_ADD(NOW(), INTERVAL 1 MONTH)\n" +
                ")\n" +
                "AND\n" +
                "step_surat in (3,5,6,7) \n" +
                "group by date(datetime_diterima)";
        return jdbcTemplate.query(sql, null, null, extractor);
    }

    public LinkedHashMap monthlySuratKeluar(){
        String sql = "SELECT date(time_created) as 'date',count(*) as 'count' from surat_keluar where date(time_created) BETWEEN\n" +
                "DATE_SUB(\n" +
                "    LAST_DAY(\n" +
                "        NOW()\n" +
                "    ), \n" +
                "    INTERVAL DAY(\n" +
                "        LAST_DAY(\n" +
                "            NOW()\n" +
                "        )\n" +
                "    )-1 DAY\n" +
                ")\n" +
                "and \n" +
                "LAST_DAY(\n" +
                "    DATE_ADD(NOW(), INTERVAL 1 MONTH)\n" +
                ")\n" +
                "\n" +
                "group by date(time_created)";
        return jdbcTemplate.query(sql, null, null, extractor);
    }

    public LinkedHashMap monthlySuratMasuk(){
        String sql = "SELECT date(datetime_diterima) as 'date',count(*) as 'count' from surat_masuk where date(datetime_diterima) BETWEEN\n" +
                "DATE_SUB(\n" +
                "    LAST_DAY(\n" +
                "        NOW()\n" +
                "    ), \n" +
                "    INTERVAL DAY(\n" +
                "        LAST_DAY(\n" +
                "            NOW()\n" +
                "        )\n" +
                "    )-1 DAY\n" +
                ")\n" +
                "and \n" +
                "LAST_DAY(\n" +
                "    DATE_ADD(NOW(), INTERVAL 1 MONTH)\n" +
                ")\n" +
//                "AND\n" +
//                "step_surat not in (3,5,6,7) \n" +
                "group by date(datetime_diterima)";
        return jdbcTemplate.query(sql, null, null, extractor);
    }
}
