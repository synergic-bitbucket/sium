package com.suim.home;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.suim.api.domain.Jabatan;
import com.suim.api.domain.SocketData;
import com.suim.config.WebSocketHandler;
import com.suim.datatables.SuratKeluar;
import com.suim.datatables.SuratMasuk;
import com.suim.mapping.DataTablesInput;
import com.suim.mapping.DataTablesOutput;
import com.suim.surat.Surat;
import com.suim.surat.SuratDAO;
import com.suim.surat.SuratMasukHandler;
import com.suim.surat.category.SuratCategoryDAO;
import com.suim.surat.search.SuratSearchDAO;
import com.suim.user.Personil;
import com.suim.user.PersonilDAO;
import com.suim.user.pangkat.PangkatDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class HomeController {

    @Autowired
    public SimpMessageSendingOperations messagingTemplate;
    @Autowired
    PersonilDAO personilDAO;
    @Autowired
    SuratDAO suratDAO;
    @Autowired
    PangkatDAO pangkatDAO;
    @Autowired
    SuratSearchDAO suratSearchDAO;
    @Autowired
    SuratCategoryDAO suratCategoryDAO;
    @Autowired
    WebSocketHandler webSocketHandler;
    @Autowired
    DashboardDAO dashboardDAO;


    @RequestMapping("/")
    public String home(Model model) {
        model.addAttribute("online", dashboardDAO.currentOnline());
        model.addAttribute("suratmasuk", dashboardDAO.todaySuratMasuk());
        model.addAttribute("suratkeluar", dashboardDAO.todaySuratKeluar());
        model.addAttribute("disposisi", dashboardDAO.todayDisposisi());

        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");

        Calendar current = Calendar.getInstance();
        current.set(Calendar.DAY_OF_MONTH,1);

        Calendar endOfMonth = Calendar.getInstance();
        endOfMonth.add(Calendar.MONTH,1);
        endOfMonth.set(Calendar.DAY_OF_MONTH,1);
        endOfMonth.add(Calendar.DAY_OF_MONTH,-1);

        LinkedHashMap disposisi = dashboardDAO.monthlyDisposisi();
        LinkedHashMap suratMasuk = dashboardDAO.monthlySuratMasuk();
        LinkedHashMap suratKeluar = dashboardDAO.monthlySuratKeluar();

        List<String> dates = new ArrayList<>();
        List dataDisposisi = new ArrayList<>();
        List dataSuratMasuk = new ArrayList<>();
        List dataSuratKeluar = new ArrayList<>();

        int i=0;
        while(i < endOfMonth.get(Calendar.DAY_OF_MONTH)){
            String date = formater.format(current.getTime());
            dates.add(date);
            dataDisposisi.add(disposisi.containsKey(date)?disposisi.get(date):0);
            dataSuratMasuk.add(suratMasuk.containsKey(date)?suratMasuk.get(date):0);
            dataSuratKeluar.add(suratKeluar.containsKey(date)?suratKeluar.get(date):0);

            current.add(Calendar.DAY_OF_MONTH,1);
            i++;
        }

        ObjectMapper objectWriter = new ObjectMapper();
        try {
            model.addAttribute("dates",objectWriter.writeValueAsString(dates));
            model.addAttribute("monthlyDisposisi", objectWriter.writeValueAsString(dataDisposisi));
            model.addAttribute("monthlySuratMasuk", objectWriter.writeValueAsString(dataSuratMasuk));
            model.addAttribute("monthlySuratKeluar", objectWriter.writeValueAsString(dataSuratKeluar));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return "home/dashboard";
    }

    @RequestMapping(value = "/suratmasuk", method = RequestMethod.GET)
    public String suratmasuk(@RequestParam(required = false, defaultValue = "") String searchQuery,
                             @RequestParam(required = false, defaultValue = "0") int searchBy, Model model) {
        model.addAttribute("title", "Surat Masuk");
        model.addAttribute("url", "suratmasuk");
        model.addAttribute("menu", 1);
        return "home/lumino-suratmasuk";

    }

    @RequestMapping(value = "/suratmasuk", method = RequestMethod.POST)
    public @ResponseBody
    DataTablesOutput<SuratMasuk> getSuratMasukDataTables(@Valid @RequestBody DataTablesInput input) {
        return suratDAO.datatablesSuratMasuk(input);
    }

    @RequestMapping(value = "/suratmasuk/distribusi",
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.POST)
    public
    @ResponseBody
    List<Jabatan> getSuratDistribusi(int id) {
        return suratCategoryDAO.getDistribusiSurat(id);
    }

    @RequestMapping(value = "/suratkeluar", method = RequestMethod.GET)
    public String suratKeluar(@RequestParam(required = false, defaultValue = "") String searchQuery,
                              @RequestParam(required = false, defaultValue = "0") int searchBy, Model model) {
//
//        String name = SecurityContextHolder.getContext().getAuthentication().getName();
//
//        model.addAttribute("name", name);
//        model.addAttribute("listSurats", suratDAO.getAllSuratKeluar());
//        model.addAttribute("listPangkats", pangkatDAO.getAll());
//        return "home/suratkeluar";
        model.addAttribute("title", "Surat Keluar");
        model.addAttribute("url", "suratkeluar");
        model.addAttribute("menu", 3);
        return "home/lumino-suratkeluar";
    }

    @RequestMapping(value = "/suratkeluar", method = RequestMethod.POST)
    public @ResponseBody
    DataTablesOutput<SuratKeluar> getSuratKeluarDataTables(@Valid @RequestBody DataTablesInput input) {
        return suratDAO.datatablesSuratKeluar(input);
    }

    @RequestMapping(value = "/disposisi", method = RequestMethod.GET)
    public String disposisi(Model model) {
        // untuk cek checkRoles yang di dapat user itu siapa aja
//        String query = suratDAO.getSuratDisposisiByRole(Personil.getRoles());
//
//        List<Surat> surats = suratDAO.getByQuery(query);
//        model.addAttribute("listSurats", surats);
//
//        return "home/lumino-suratmasuk";
        model.addAttribute("title", "Disposisi");
        model.addAttribute("url", "disposisi");
        model.addAttribute("menu", 2);
        return "home/lumino-suratmasuk";
    }

    @RequestMapping(value = "/disposisi", method = RequestMethod.POST)
    public @ResponseBody
    DataTablesOutput<SuratMasuk> getDisposisiDataTables(@Valid @RequestBody DataTablesInput input) {
        return suratDAO.datatablesDisposisi(input);
    }


    @RequestMapping(value = "/distribusi",
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.POST)
    public
    @ResponseBody
    Map distribusiDisposisi(int id, String jabatan) {
        Map data = new HashMap<>();
        data.put("status", false);
        ObjectMapper mapper = new ObjectMapper();
        try {
            List<Jabatan> listJabatan = mapper.readValue(jabatan, new TypeReference<List<Jabatan>>() {
            });
            boolean status = suratCategoryDAO.distribusikan(id, listJabatan);
            data.put("status", status);
            String jsonString = new ObjectMapper().writeValueAsString(suratDAO.getById(id));
            if (status) {
                for (Jabatan j : listJabatan) {
                    messagingTemplate.convertAndSend("/disposisi/jabatan/" + j.getId_jabatan(), jsonString);
                    webSocketHandler.sendToJabatan(j.getId_jabatan() + "", new SocketData(SocketData.TYPE_DISPOSISI, jsonString));
                }
                messagingTemplate.convertAndSend("/disposisi/jabatan/" + Personil.USER_SRIPIM, jsonString);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }

}
