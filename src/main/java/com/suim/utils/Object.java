package com.suim.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by raizal.pregnanta on 01/11/2016.
 */
public class Object {

    public Map<String,String> toMap(){
        Method[] methods = getClass().getMethods();
        Map<String, String> map = new HashMap<String, String>();
        for(Method m : methods)
        {
            if(m.getName().startsWith("get") && m.getName().indexOf("getClass")==-1)
            {
                String value = null;
                try {
                    value = m.invoke(this).toString();

                    String name = m.getName().substring(3);
                    name = name.substring(0,1).toLowerCase()+name.substring(1);
                    System.out.println(name+" = "+value);
                    map.put(name, value);
                } catch (IllegalAccessException e) {

                } catch (InvocationTargetException e) {

                }catch (NullPointerException e){

                }
            }
        }
        return map;
    }
}
