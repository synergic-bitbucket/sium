package com.suim.surat;

import org.springframework.jdbc.core.RowMapper;

public class SuratMasukHandler {

	private String query;
	private RowMapper<Surat> rowmapper;
	
	public SuratMasukHandler() {
		// TODO Auto-generated constructor stub
	}
	
	public SuratMasukHandler(String query,RowMapper<Surat> surat){
		this.query = query;
		this.rowmapper = surat;
	}
	
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public RowMapper<Surat> getRowmapper() {
		return rowmapper;
	}
	public void setRowmapper(RowMapper<Surat> rowmapper) {
		this.rowmapper = rowmapper;
	}
	
}
