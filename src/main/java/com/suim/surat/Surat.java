package com.suim.surat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.suim.api.domain.Jabatan;
import com.suim.utils.Object;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Surat extends Object {
    private int id_surat, step_surat;
    private String datetime_diterima,
            file_surat, disposisi, waktu_disposisi, disposisi_oleh, ket_lain, ket_kasium, file_disposisi, tanggal_undangan = "0000-00-00", batas_waktu;

    @Valid
    @NotNull
    private String no_agenda, dari, no_surat, tanggal_surat, jenis_surat, perihal;

    private List<Jabatan> penerima = new ArrayList<>();

    public int getId_surat() {
        return id_surat;
    }

    public void setId_surat(int id_surat) {
        this.id_surat = id_surat;
    }

    public int getStep_surat() {
        return step_surat;
    }

    public void setStep_surat(int step_surat) {
        this.step_surat = step_surat;
    }

    public String getNo_agenda() {
        return no_agenda;
    }

    public void setNo_agenda(String no_agenda) {
        this.no_agenda = no_agenda;
    }

    public String getDatetime_diterima() {
        return datetime_diterima;
    }

    public void setDatetime_diterima(String datetime_diterima) {
        this.datetime_diterima = datetime_diterima;
    }

    public String getNo_surat() {
        return no_surat;
    }

    public void setNo_surat(String no_surat) {
        this.no_surat = no_surat;
    }

    public String getDari() {
        return dari;
    }

    public void setDari(String dari) {
        this.dari = dari;
    }

    public String getTanggal_surat() {
        return tanggal_surat;
    }

    public void setTanggal_surat(String tanggal_surat) {
        this.tanggal_surat = tanggal_surat;
    }

    public String getPerihal() {
        return perihal;
    }

    public void setPerihal(String perihal) {
        this.perihal = perihal;
    }

    public String getPerihalSubString() {
        return (perihal.length() > 120 ? perihal.substring(0, 120) + "..." : perihal);
    }

    public String getJenis_surat() {
        return jenis_surat;
    }

    public void setJenis_surat(String jenis_surat) {
        this.jenis_surat = jenis_surat;
    }

    public String getKet_kasium() {
        return ket_kasium;
    }

    public void setKet_kasium(String ket_kasium) {
        this.ket_kasium = ket_kasium;
    }

    public String getKet_lain() {
        return ket_lain;
    }

    public void setKet_lain(String ket_lain) {
        this.ket_lain = ket_lain;
    }

    public String getFile_surat() {
        return file_surat;
    }

    public void setFile_surat(String file_surat) {
        this.file_surat = file_surat;
    }

    public String getDisposisi() {
        return disposisi;
    }

    public void setDisposisi(String disposisi) {
        this.disposisi = disposisi;
    }

    public String getFile_disposisi() {
        return file_disposisi;
    }

    public void setFile_disposisi(String file_disposisi) {
        this.file_disposisi = file_disposisi;
    }

    public String getWaktu_disposisi() {
        return waktu_disposisi;
    }

    public void setWaktu_disposisi(String waktu_disposisi) {
        this.waktu_disposisi = waktu_disposisi;
    }

    public String getDisposisi_oleh() {
        return disposisi_oleh;
    }

    public void setDisposisi_oleh(String disposisi_oleh) {
        this.disposisi_oleh = disposisi_oleh;
    }

    public String getTanggal_undangan() {
        return tanggal_undangan;
    }

    public void setTanggal_undangan(String tanggal_undangan) {
        this.tanggal_undangan = tanggal_undangan;
    }

    public String getBatas_waktu() {
        return batas_waktu;
    }

    public void setBatas_waktu(String batas_waktu) {
        this.batas_waktu = batas_waktu;
    }

    @Override
    public Map<String, String> toMap() {
        Map map = super.toMap();

        map.remove("is_alowed");
        map.remove("perihalSubString");
        return map;
    }

    public List<Jabatan> getPenerima() {
        return penerima;
    }

    public void setPenerima(List<Jabatan> penerima) {
        this.penerima.addAll(penerima);
    }
}
