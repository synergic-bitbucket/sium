package com.suim.surat;

import com.suim.SQLWrapper;
import com.suim.datatables.SuratMasuk;
import com.suim.mapping.Column;
import com.suim.mapping.DataTablesInput;
import com.suim.mapping.DataTablesOutput;
import com.suim.mapping.Order;
import com.suim.notification.NotificationHandler;
import com.suim.resource.DAOInterface;
import com.suim.user.Personil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Service("SuratDAO")
public class SuratDAO implements DAOInterface<Surat>{

	public static final int TERUSKAN_KA = 4;
	public static final int DISPOSISI_KA = 5;
	public static final int DISPOSISI_WAKA = 3;
	public static final int TERUSKA_WAKA = 2;
	public static final int DIEDIT_SRIPIM = 6;
	public static final int TELAH_DIDISTRIBUSIKAN = 7;

	@Autowired
	JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	private SimpMessagingTemplate brokerMessagingTemplate;


	public static final String selectQuery = "id_surat, batas_waktu, no_agenda, datetime_diterima, no_surat, dari, tanggal_surat, "
			+ "perihal, js.nama_jenis as jenis_surat, ket_kasium, ket_lain, file_surat, disposisi, "
			+ "file_disposisi, waktu_disposisi, disposisi_oleh, step_surat,tanggal_undangan ";

	public static final String joinQuery = "inner join jenis_surat js on surat_masuk.jenis_surat = js.kode_surat  ";

	@Override
	public Surat getById(int id) {
		String sql = "select * from surat_masuk where id_surat = ?";
		return (Surat) jdbcTemplate.queryForObject(sql, new Object[]{ id },new SuratFullMapper());
	}

	public Surat getByIdDetail(int id){
		String sql = "SELECT  id_surat,batas_waktu, no_agenda, datetime_diterima, no_surat, dari, tanggal_surat, "
				+ "perihal, js.nama_jenis as jenis_surat, ket_kasium, ket_lain, file_surat, disposisi, "
				+ "file_disposisi, waktu_disposisi, disposisi_oleh, step_surat,tanggal_undangan FROM surat_masuk "
				+ "inner join jenis_surat js on surat_masuk.jenis_surat = js.kode_surat  where id_surat = ?";
		return (Surat) jdbcTemplate.queryForObject(sql, new Object[]{ id },new SuratFullMapper());
	}


    public boolean insertSuratKeluar(SuratKeluar suratKeluar){
        Map data = suratKeluar.toMap();
        return namedParameterJdbcTemplate.update(SQLWrapper.insert("surat_keluar",data).getSQL(),data)>0;
    }

    public boolean updateSuratKeluar(SuratKeluar suratKeluar){
        Map data = suratKeluar.toMap();
        int id = Integer.valueOf(data.get("id_surat").toString());
        data.remove("id_surat");
        return namedParameterJdbcTemplate.update(SQLWrapper.where(" id_surat="+id).update("surat_keluar",data).getSQL(),data)>0;
    }

    public boolean deleteSuratKeluar(SuratKeluar suratKeluar){
        Map data = suratKeluar.toMap();
        int id = Integer.valueOf(data.get("id_surat").toString());
        data.remove("id_surat");
        return namedParameterJdbcTemplate.update(SQLWrapper.where(" id_surat="+id).delete("surat_keluar").getSQL(),data)>0;
    }

    public SuratKeluar getSuratKeluarById(int id){
        return jdbcTemplate.queryForObject("select * from surat_keluar a,jenis_surat_keluar b where a.jenis_surat=b.kode_surat and a.id_surat = "+id+" ", new Object[]{},new SuratKeluarMapper());
    }

	@Override
	public List<Surat> getByQuery(String query) {
		// TODO Auto-generated method stub
		return jdbcTemplate.query(query, new SuratFullMapper());
	}

	public List<Surat> getByQuery(String query,RowMapper<Surat> rowMapper) {
		// TODO Auto-generated method stub
		return jdbcTemplate.query(query, rowMapper);
	}

	public List<Surat> getSearch(String query,String  object,String whereClause) {
		// TODO Auto-generated method stub
		query = "select * from surat_masuk "+query;
		if( whereClause.trim().length() > 0 ){
			query += " and "+whereClause;
		}
		String queryResult = query.replace("?", object);
		System.out.println(queryResult);
		return jdbcTemplate.query(queryResult,new SuratFullMapper());
	}

    public boolean teruskan(int id){
        return namedParameterJdbcTemplate.update("update surat_masuk set step_surat=4 where id_surat="+id, new HashMap<String, Object>()) >0;
    }

    public boolean checkDistribusi(int surat){
        return namedParameterJdbcTemplate.queryForRowSet("select * from penerima_disposisi where id_surat="+surat+" and id_jabatan="+Personil.getRoles(),new HashMap<String, Object>()).first();
    }

	@Override
	public boolean update(int id, Surat data) {

        Map surat = data.toMap();
        surat.remove("step_surat");
        surat.remove("disposisi");
        surat.remove("waktu_disposisi");
        surat.remove("file_disposisi");
        String sql = SQLWrapper.where(" id_surat ="+id).update("surat_masuk",surat).getSQL();
        System.out.println(sql);
        return namedParameterJdbcTemplate.update(sql,surat)>0;

	}

    @Override
    public boolean delete(int id) {
        return false;
    }

//    public boolean updateSurat(Surat data){
//		// TODO Auto-generated method stub
//				String sql = "UPDATE surat_masuk SET no_agenda=?,datetime_diterima=?,no_surat=?,dari=?,tanggal_surat=?,perihal=?,jenis_surat=?,ket_kasium=?,ket_lain=?,"
//						+ "file_surat=?,disposisi=?,file_disposisi=?,waktu_disposisi=?,disposisi_oleh=?,step_surat=? WHERE id_surat = ?";
//				return jdbcTemplate.update(sql,new Object[]{data.getNo_agenda(),data.getDatetime_diterima(),
//						data.getNo_surat(),data.getDari(),data.getTanggal_surat(),data.getPerihal(),
//						data.getJenis_surat(),data.getKet_kasium(),data.getKet_lain(),
//						data.getFile_surat(),data.getDisposisi(),data.getFile_disposisi(),data.getWaktu_disposisi(),
//						data.getDisposisi_oleh(),DIEDIT_SRIPIM,data.getId_surat()}) > 0;
//	}


	public boolean disposisiSuratMasuk(int id,String disposisi){
        String roles = Personil.getRoles();
        Map data = new HashMap<>();
        data.put("disposisi",disposisi);
        data.put("step_surat",(roles.equals(Personil.USER_KA)?"5":roles.equals(Personil.USER_WAKA)?"3":"6"));
        data.put("waktu_disposisi=NOW()",null);
        String query = SQLWrapper.update("surat_masuk",data).where("id_surat="+id).getSQL();
        return namedParameterJdbcTemplate.update(query,data)>0;
    }

    public boolean updateDisposisiAjudan(Surat surat){
        Map data = new HashMap<>();
        data.put("disposisi",surat.getDisposisi());
        data.put("file_disposisi",surat.getFile_disposisi());
        data.put("step_surat",6);
        data.put("waktu_disposisi=NOW()",null);
        String sql =SQLWrapper.where("id_surat="+surat.getId_surat()).update("surat_masuk",data).getSQL();
        System.out.println( sql);
        return namedParameterJdbcTemplate.update(sql,data)>0;
    }

	public boolean updateDisposisi(Surat surat){
		String auth = "0";
		for (SimpleGrantedAuthority simpleGrantedAuthority : (Collection<SimpleGrantedAuthority>) SecurityContextHolder.
				getContext().getAuthentication().
				getAuthorities()) {
			auth = simpleGrantedAuthority.getAuthority();
			break;
		}
		return updateDisposisi(surat, SecurityContextHolder.getContext()
				.getAuthentication().getName(), auth);

	}

	public boolean updateDisposisi(Surat surat,String user,String roles){
		int step = roles.equals(Personil.USER_KA) ? SuratDAO.DISPOSISI_KA : SuratDAO.DISPOSISI_WAKA;
		if(jdbcTemplate.update("UPDATE surat_masuk SET disposisi=?,file_disposisi=?,waktu_disposisi=NOW(),disposisi_oleh=?,step_surat = ? WHERE id_surat = ?"
				,surat.getDisposisi(),surat.getFile_disposisi(),user,step,surat.getId_surat()) > 0){
			return true;
		}
		return false;
	}

	public boolean deleteSuratMasuk(int id) {
		return jdbcTemplate.update("delete from surat_masuk where id_surat="+id)>0;
	}

    public boolean deleteSuratKeluar(int id) {
        return jdbcTemplate.update("delete from surat_keluar where id_surat="+id)>0;
    }

	@Override
	public boolean create(Surat data) {
		// TODO Auto-generated method stub
		try{
			String sql = "INSERT INTO surat_masuk(no_agenda, datetime_diterima, no_surat, dari, tanggal_surat, perihal, jenis_surat, ket_kasium, ket_lain, file_surat,step_surat,batas_waktu,tanggal_undangan) VALUES (?,NOW(),?,?,?,?,?,?,?,?,?,?,?)";
			int rowAffected = jdbcTemplate.update(sql,data.getNo_agenda(),data.getNo_surat(),
					data.getDari(),data.getTanggal_surat(),data.getPerihal(),data.getJenis_surat(),data.getKet_kasium(),data.getKet_lain(),
					data.getFile_surat(),data.getStep_surat(),data.getBatas_waktu(),data.getTanggal_undangan());

			sendMessage(Personil.USER_ADMIN,false);

			if( rowAffected > 0 ){
				return true;
			}

			return false;
		}catch( Exception e ){
			e.printStackTrace();
		}
		return false;
	}


	@Override
	public List<Surat> getAll(int start, int limit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Surat> getAll() {
		String sql = "SELECT  id_surat, no_agenda, datetime_diterima, no_surat, dari, tanggal_surat, "
				+ "perihal, js.nama_jenis as jenis_surat, ket_kasium, ket_lain, file_surat, disposisi, "
				+ "file_disposisi, waktu_disposisi, disposisi_oleh, step_surat FROM surat_masuk "
				+ "inner join jenis_surat js on surat_masuk.jenis_surat = js.kode_surat";
		return jdbcTemplate.query(sql, new SuratFullMapper());
	}

	public List<SuratKeluar> getAllSuratKeluar() {
		String sql = "SELECT  * from surat_keluar "
				+ "inner join jenis_surat_keluar js on surat_keluar.jenis_surat = js.kode_surat order by tanggal_surat DESC";
		return jdbcTemplate.query(sql, new SuratKeluarMapper());
	}

	public String getSuratKeluar(){
        String sql = "SELECT  * from surat_keluar "
                + "inner join jenis_surat_keluar js on surat_keluar.jenis_surat = js.kode_surat";
        return sql;
    }

    public String getSuratKeluar(String where){
        return getSuratKeluar()+" "+where;
    }

    public String getSuratKeluar(String where, int page, int pageCount,String orderColumn,String direction){
        return getSuratKeluar(where)+" order by "+orderColumn+" "+direction+" limit "+( (page-1)*pageCount)+","+( (page)*pageCount );
    }


    private List<com.suim.datatables.SuratKeluar> castToDatatablesSuratKeluar(List<SuratKeluar> surat){
        List<com.suim.datatables.SuratKeluar> result = new ArrayList<>();
        for(SuratKeluar s : surat){
            result.add(new com.suim.datatables.SuratKeluar(s));
        }
        return result;
    }

    public DataTablesOutput<com.suim.datatables.SuratKeluar> datatablesSuratKeluar(DataTablesInput input){

        StringBuilder builder = new StringBuilder();
        for(Column col : input.getColumns()){
            if(!col.getSearchable() || col.getSearch().getValue().equals("")) continue;

            if(builder.length()>0)
                builder.append(" and ");

            if(col.getName().equals("jenis_surat")){
                builder.append("surat_keluar."+col.getName()+"=\""+col.getSearch().getValue()+"\"");
            }else
                builder.append("surat_keluar."+col.getName()+" like \"%"+col.getSearch().getValue()+"%\"");
        }
        if(builder.toString().length()>0) {
            builder.append(")");
            builder.insert(0, " and (");
        }
        String where  = builder.toString();

        DataTablesOutput<com.suim.datatables.SuratKeluar> output = new DataTablesOutput<>();
        String query = getSuratKeluar(where, (input.getStart()+input.getLength())/input.getLength(), input.getLength(), input.getColumns().get(input.getOrder().get(0).getColumn()).getName() ,  input.getOrder().get(0).getDir());

        output.setData(castToDatatablesSuratKeluar(jdbcTemplate.query(query, new SuratKeluarMapper())));

        output.setDraw(input.getDraw());

        String queryFiltere = getSuratKeluar();

        output.setRecordsFiltered(jdbcTemplate.query(getSuratKeluar(where), new SuratKeluarMapper()).size());

        output.setRecordsTotal(jdbcTemplate.query(getSuratKeluar(), new SuratKeluarMapper()).size());

        return output;
    }

//	class SuratDetailMapper implements RowMapper<Surat>{
//
//		@Override
//		public Surat mapRow(ResultSet rs, int rowNum) throws SQLException {
//			Surat surat = new Surat();
//			surat.setDatetime_diterima( rs.getString("datetime_diterima") );
//			surat.setJenis_surat( rs.getString("jenis_surat") );
//			surat.setDari( rs.getString("dari") );
//			surat.setPerihal( rs.getString("perihal") );
//			surat.setId_surat( rs.getInt("id_surat") );
//			surat.setNo_agenda( rs.getString("no_agenda") );
//			surat.setNo_surat( rs.getString("no_surat") );
//			surat.setKet_kasium( rs.getString("ket_kasium") );
//			surat.setKet_lain( rs.getString("ket_lain") );
//			surat.setFile_surat( rs.getString("file_surat"));
//			return surat;
//		}
//		
//	}
//	

	public static class SuratFullMapper implements RowMapper<Surat>{

		@Override
		public Surat mapRow(ResultSet rs, int rowNum) throws SQLException {
			Surat surat = new Surat();
			surat.setDatetime_diterima( rs.getString("datetime_diterima") );
			surat.setJenis_surat( rs.getString("jenis_surat") );
			surat.setDari( rs.getString("dari") );
			surat.setPerihal( rs.getString("perihal") );
			surat.setId_surat( rs.getInt("id_surat") );
			surat.setNo_agenda( rs.getString("no_agenda") );
			surat.setNo_surat( rs.getString("no_surat") );
			surat.setKet_kasium( rs.getString("ket_kasium") );
			surat.setKet_lain( rs.getString("ket_lain") );
			surat.setFile_surat( rs.getString("file_surat"));
			surat.setStep_surat( rs.getInt("step_surat") );
			surat.setDisposisi( rs.getString("disposisi") );
			surat.setDisposisi_oleh( rs.getString("disposisi_oleh") );
			surat.setFile_disposisi( rs.getString("file_disposisi") );
			surat.setWaktu_disposisi( rs.getString("waktu_disposisi") );
			surat.setTanggal_surat( rs.getString("tanggal_surat") );
            surat.setBatas_waktu(rs.getString("batas_waktu"));
            surat.setTanggal_undangan(rs.getString("tanggal_undangan"));
			return surat;
		}

	}


 	public  static class DisposisiSuratMapper implements RowMapper<Surat>{

		@Override
		public Surat mapRow(ResultSet rs, int rowNum) throws SQLException {
			Surat surat = new Surat();
			surat.setDatetime_diterima( rs.getString("datetime_diterima") );
			surat.setJenis_surat( rs.getString("jenis_surat") );
			surat.setDari( rs.getString("dari") );
			surat.setPerihal( rs.getString("perihal") );
			surat.setId_surat( rs.getInt("id_surat") );
			surat.setNo_agenda( rs.getString("no_agenda") );
			surat.setNo_surat( rs.getString("no_surat") );
			surat.setKet_kasium( rs.getString("ket_kasium") );
			surat.setKet_lain( rs.getString("ket_lain") );
			surat.setFile_surat( rs.getString("file_surat"));
			surat.setStep_surat( rs.getInt("step_surat") );
			surat.setDisposisi( rs.getString("disposisi") );
			surat.setDisposisi_oleh( rs.getString("disposisi_oleh") );
			surat.setFile_disposisi( rs.getString("file_disposisi") );
			surat.setWaktu_disposisi( rs.getString("waktu_disposisi") );
			surat.setTanggal_surat( rs.getString("tanggal_surat") );
			return surat;
		}

	}

	public static class SuratKeluarMapper implements RowMapper<SuratKeluar>{

		@Override
		public SuratKeluar mapRow(ResultSet rs, int rowNum) throws SQLException {
			SuratKeluar surat = new SuratKeluar();
			surat.setJenis_surat( rs.getString("nama_jenis") );
			surat.setKepada( rs.getString("kepada") );
			surat.setPerihal( rs.getString("perihal") );
			surat.setId_surat( rs.getInt("id_surat") );
			surat.setNo_surat( rs.getString("no_surat") );
			surat.setKet_lain( rs.getString("ket_lain") );
			surat.setFile_surat( rs.getString("file_surat"));
			surat.setTanggal_surat( rs.getString("tanggal_surat") );
			return surat;
		}

	}

	public String getSuratDisposisiByRole(String roles){
		String query = "";

		switch( roles ){
			case Personil.USER_KA :;
			case Personil.USER_WAKA:
				query = "select "+SuratDAO.selectQuery+" from surat_masuk "+SuratDAO.joinQuery+" where step_surat = "+SuratDAO.TELAH_DIDISTRIBUSIKAN;
				break;
			case Personil.USER_SRIPIM:
				query = "select "+SuratDAO.selectQuery+",(step_surat = "+String.valueOf(SuratDAO.DISPOSISI_WAKA)+" or step_surat = "+String.valueOf(SuratDAO.DISPOSISI_KA)+" or "
						+ "step_surat = "+String.valueOf( SuratDAO.DIEDIT_SRIPIM )+") as is_alowed from surat_masuk "+SuratDAO.joinQuery+" where step_surat = "+SuratDAO.TELAH_DIDISTRIBUSIKAN;
				break;
			case Personil.USER_ADMIN :
				query = "select "+SuratDAO.selectQuery+" from surat_masuk "+SuratDAO.joinQuery+" where step_surat = "+SuratDAO.TELAH_DIDISTRIBUSIKAN;
				break;
			default :
				query = "SELECT "+SuratDAO.selectQuery+" FROM surat_masuk "+SuratDAO.joinQuery+" inner join penerima_disposisi pd on pd.id_surat_masuk = "
						+ "surat_masuk.id_surat and pd.id_jabatan="+roles
						+ " where step_surat = "+SuratDAO.TELAH_DIDISTRIBUSIKAN;
				break;
		}
		return query;
	}

    public String getSuratDisposisiByRole(String role,String jenis,String where,String orderColumn,String direction){
        String query = getSuratDisposisiByRole(role);
        return query+" "+(jenis.equals("all")?"":"and kode_surat=\""+jenis+"\"")+" "+where+" order by "+orderColumn+" "+direction;
    }

    public String getSuratDisposisiByRole(String role,String jenis,int page, int pageCount){
        String query = getSuratDisposisiByRole(role);
        return query+" "+(jenis.equals("all")?"":"and kode_surat=\""+jenis+"\"")+" order by datetime_diterima DESC";
    }

    public String getSuratDisposisiByRole(String role,String jenis,String where, int page, int pageCount,String orderColumn,String direction){
        return getSuratDisposisiByRole(role,jenis,where,orderColumn,direction)+" limit "+( (page-1)*pageCount)+","+( (page)*pageCount );
    }

    private List<SuratMasuk> castToDatatablesSuratMasuk(List<Surat> surat){
        List<SuratMasuk> result = new ArrayList<>();
        for(Surat s : surat){
            result.add(new SuratMasuk(s));
        }
        return result;
    }

    public DataTablesOutput<SuratMasuk> datatablesDisposisi(DataTablesInput input){

        StringBuilder builder = new StringBuilder();
        for(Column col : input.getColumns()){
            if(!col.getSearchable() || col.getSearch().getValue().equals("")) continue;

            if(builder.length()>0)
                builder.append(" and ");

            if(col.getName().equals("jenis_surat")){
                builder.append("surat_masuk."+col.getName()+"=\""+col.getSearch().getValue()+"\"");
            }else
                builder.append("surat_masuk."+col.getName()+" like \"%"+col.getSearch().getValue()+"%\"");
        }
        if(builder.toString().length()>0) {
            builder.append(")");
            builder.insert(0, " and (");
        }
        String where  = builder.toString();

        DataTablesOutput<SuratMasuk> output = new DataTablesOutput<>();
        String query = getSuratDisposisiByRole(Personil.getRoles(), "all",where, (input.getStart()+input.getLength())/input.getLength(), input.getLength(), input.getColumns().get(input.getOrder().get(0).getColumn()).getName() ,  input.getOrder().get(0).getDir());

        System.out.println(query);

        output.setData(castToDatatablesSuratMasuk(getByQuery(query, new SuratDAO.SuratFullMapper())));

        output.setDraw(input.getDraw());

        String queryFiltere = getSuratDisposisiByRole(Personil.getRoles(), "all",where, input.getColumns().get(input.getOrder().get(0).getColumn()).getName() ,  input.getOrder().get(0).getDir());;

        output.setRecordsFiltered( getByQuery(queryFiltere, new SuratDAO.SuratFullMapper()).size() );

        output.setRecordsTotal(getByQuery("select * from surat_masuk", new SuratDAO.SuratFullMapper()).size());

        return output;
    }

    public DataTablesOutput<SuratMasuk> datatablesSuratMasuk(DataTablesInput input){

        StringBuilder builder = new StringBuilder();
        for(Column col : input.getColumns()){
            if(!col.getSearchable() || col.getSearch().getValue().equals("")) continue;

            if(builder.length()>0)
                builder.append(" and ");

            if(col.getName().equals("jenis_surat")){
                builder.append("surat_masuk."+col.getName()+"=\""+col.getSearch().getValue()+"\"");
            }else
                builder.append("surat_masuk."+col.getName()+" like \"%"+col.getSearch().getValue()+"%\"");
        }
        if(builder.toString().length()>0) {
            builder.append(")");
            builder.insert(0, " and (");
        }
        String where  = builder.toString();

        DataTablesOutput<SuratMasuk> output = new DataTablesOutput<>();
        SuratMasukHandler query = getSuratByRole(Personil.getRoles(), "all",where, (input.getStart()+input.getLength())/input.getLength(), input.getLength(), input.getColumns().get(input.getOrder().get(0).getColumn()).getName() ,  input.getOrder().get(0).getDir());

        output.setData(castToDatatablesSuratMasuk(getByQuery(query.getQuery(), new SuratDAO.SuratFullMapper())));

        output.setDraw(input.getDraw());

        String queryFiltere = getSuratByRole(Personil.getRoles(),"all",where).getQuery();

        output.setRecordsFiltered( getByQuery(queryFiltere, new SuratDAO.SuratFullMapper()).size() );

        output.setRecordsTotal(getByQuery("select * from surat_masuk", new SuratDAO.SuratFullMapper()).size());

        return output;
    }

	public SuratMasukHandler getSuratByRole(String role){
		String query = "";
		RowMapper<Surat> rowMapper = null;
		switch( role ){
			case Personil.USER_KA :;
			case Personil.USER_WAKA:
				role = role.equals(Personil.USER_KA) ? String.valueOf(SuratDAO.TERUSKAN_KA)
						: String.valueOf(SuratDAO.TERUSKA_WAKA);
				query = "select "+SuratDAO.selectQuery+" from surat_masuk "+SuratDAO.joinQuery+" where step_surat = "+role;

				rowMapper = new SuratFullMapper();
				break;
			case Personil.USER_ADMIN:;
                query = "select "+SuratDAO.selectQuery+" from surat_masuk "+SuratDAO.joinQuery+" where step_surat = "+2;
                rowMapper = new DisposisiSuratMapper();
                break;
            case Personil.USER_SRIPIM:
				query = "select "+SuratDAO.selectQuery+",(step_surat = "+SuratDAO.DISPOSISI_KA+" "
						+ "or step_surat = "+SuratDAO.DISPOSISI_WAKA+" ) as is_alowed from surat_masuk "+SuratDAO.joinQuery;
				rowMapper = new DisposisiSuratMapper();
				break;
			default :
				query = "select a.* from surat_masuk a, penerima_disposisi b where a.id_surat = b.id_surat and b.id_jabatan="+role;
				rowMapper = new DisposisiSuratMapper();
				break;
		}
		return new SuratMasukHandler(query+" order by datetime_diterima DESC", rowMapper);
	}

    public SuratMasukHandler getSuratByRole(String role,String jenis, String where){
        String query = "";
        RowMapper<Surat> rowMapper = null;
        switch( role ){
            case Personil.USER_KA :;
            case Personil.USER_WAKA:
                role = role.equals(Personil.USER_KA) ? String.valueOf(SuratDAO.TERUSKAN_KA)
                        : String.valueOf(SuratDAO.TERUSKA_WAKA);
                query = "select "+SuratDAO.selectQuery+" from surat_masuk "+SuratDAO.joinQuery+" where step_surat = "+role;

                rowMapper = new SuratFullMapper();
                break;
            case Personil.USER_ADMIN:;
                query = "select "+SuratDAO.selectQuery+" from surat_masuk "+SuratDAO.joinQuery+" where step_surat = "+2;
                rowMapper = new DisposisiSuratMapper();
                break;
            case Personil.USER_SRIPIM:
                query = "select "+SuratDAO.selectQuery+",(step_surat = "+SuratDAO.DISPOSISI_KA+" "
                        + "or step_surat = "+SuratDAO.DISPOSISI_WAKA+" ) as is_alowed from surat_masuk "+SuratDAO.joinQuery;
                rowMapper = new DisposisiSuratMapper();
                break;
            default :
                query = "select a.* from surat_masuk a, penerima_disposisi b where a.id_surat = b.id_surat and b.id_jabatan="+role;
                rowMapper = new DisposisiSuratMapper();
                break;
        }
        query = query+" "+(jenis.equals("all")?"":"and kode_surat=\""+jenis+"\"")+" "+where+" order by datetime_diterima DESC ";
        return new SuratMasukHandler(query, rowMapper);
    }

    public SuratMasukHandler getSuratByRole(String role,String jenis,String where, int page, int pageCount,String orderColumn,String direction){
        String query = "";
        RowMapper<Surat> rowMapper = null;
        switch( role ){
            case Personil.USER_KA :;
            case Personil.USER_WAKA:
                role = role.equals(Personil.USER_KA) ? String.valueOf(SuratDAO.TERUSKAN_KA)
                        : String.valueOf(SuratDAO.TERUSKA_WAKA);
                query = "select "+SuratDAO.selectQuery+" from surat_masuk "+SuratDAO.joinQuery+" where step_surat = "+role;

                rowMapper = new SuratFullMapper();
                break;
            case Personil.USER_ADMIN:;
                query = "select "+SuratDAO.selectQuery+" from surat_masuk "+SuratDAO.joinQuery+" where step_surat = "+2;
                rowMapper = new DisposisiSuratMapper();
                break;
            case Personil.USER_SRIPIM:
                query = "select "+SuratDAO.selectQuery+",(step_surat = "+SuratDAO.DISPOSISI_KA+" "
                        + "or step_surat = "+SuratDAO.DISPOSISI_WAKA+" ) as is_alowed from surat_masuk "+SuratDAO.joinQuery;
                rowMapper = new DisposisiSuratMapper();
                break;
            default :
                query = "select a.* from surat_masuk a, penerima_disposisi b where a.id_surat = b.id_surat and b.id_jabatan="+role;
                rowMapper = new DisposisiSuratMapper();
                break;
        }
        query = query+" "+(jenis.equals("all")?"":"and kode_surat=\""+jenis+"\"")+" "+where+" order by "+orderColumn+" "+direction+" limit "+( (page-1)*pageCount)+","+( (page)*pageCount );
        return new SuratMasukHandler(query, rowMapper);
    }


	private void sendMessage(String idJabatan,boolean forSripim){
		try{
			NotificationHandler.ResponseStep responseStep = NotificationHandler.getStep(
						String.valueOf( idJabatan ),forSripim);
			for( String jabatan : responseStep.jabatan ){
				this.brokerMessagingTemplate.convertAndSend(NotificationHandler.PREFIX_MESSAGE+jabatan+NotificationHandler.MESSAGE_DISPOSISI, responseStep.msg);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

    public int countSuratMasuk(String jenis, String role){
        String condition = "";
        String jenisSurat = jenis.equals("all")?"":"jenis_surat=\""+jenis+"\" and";
        switch (role){
            case Personil.USER_SRIPIM:
                condition = "step_surat < 7";
                break;
            case Personil.USER_KA:
                condition = "step_surat in (4)";
                break;
            case Personil.USER_WAKA:
                condition = "step_surat in (2)";
                break;
            default:
                condition = "id_surat in (select id_surat_masuk as id_surat from penerima_disposisi where id_jabatan="+role+")";
                return 0;
        }
        Map data = jdbcTemplate.queryForMap("select count(*) as c from surat_masuk where "+jenisSurat+" "+condition+ " order by datetime_diterima DESC");
        if(data.size()>0 && data.containsKey("c")){
            return Integer.valueOf(data.get("c").toString());
        }
        return 0;
    }

    public int countDisposisi(String jenis, String role){
        String condition = "";
        String jenisSurat = jenis.equals("all")?"":"jenis_surat=\""+jenis+"\" and";
        switch (role){
            case Personil.USER_SRIPIM:
            case Personil.USER_KA:
            case Personil.USER_WAKA:
                condition = "step_surat in (7)";
                break;
            default:
                condition = "id_surat in (select id_surat_masuk as id_surat from penerima_disposisi where id_jabatan="+role+")";
                break;
        }
        Map data = jdbcTemplate.queryForMap("select count(*) as c from surat_masuk where "+jenisSurat+" "+condition+ " order by waktu_disposisi DESC");
        if(data.size()>0 && data.containsKey("c")){
            return Integer.valueOf(data.get("c").toString());
        }
        return 0;
    }
}
