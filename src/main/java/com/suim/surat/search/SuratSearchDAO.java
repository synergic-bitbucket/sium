package com.suim.surat.search;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.suim.resource.DAOInterface;

@Service("SuratSearchDAO")
public class SuratSearchDAO implements DAOInterface<SuratSearch>{

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Override
	public SuratSearch getById(int id) {
		return jdbcTemplate.queryForObject("select * from surat_search where id = ?", 
				new Object[]{ id },new SuratSearchRowMapper());
	}

	@Override
	public List<SuratSearch> getByQuery(String query) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SuratSearch> getAll(int start, int limit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SuratSearch> getAll() {
		// TODO Auto-generated method stub
		return jdbcTemplate.query("select * from surat_search", new SuratSearchRowMapper());
	}

	@Override
	public boolean update(int id, SuratSearch data) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean create(SuratSearch data) {
		// TODO Auto-generated method stub
		return false;
	}
	
	class SuratSearchRowMapper implements RowMapper<SuratSearch>{

		@Override
		public SuratSearch mapRow(ResultSet rs, int rowNum) throws SQLException {
			SuratSearch suratSearch = new SuratSearch();
			suratSearch.setId( rs.getInt("id") );
			suratSearch.setName( rs.getString("name") );
			suratSearch.setQuery( rs.getString("query") );
			return suratSearch;
		}
		
	}

}
