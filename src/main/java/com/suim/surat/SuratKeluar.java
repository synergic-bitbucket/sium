package com.suim.surat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.suim.utils.Object;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SuratKeluar extends Object{
    private int id_surat;
    private String
            file_surat, ket_lain;

    @Valid
    @NotNull
    private String kepada, no_surat, tanggal_surat, jenis_surat, perihal;

    public int getId_surat() {
        return id_surat;
    }

    public void setId_surat(int id_surat) {
        this.id_surat = id_surat;
    }


    public String getNo_surat() {
        return no_surat;
    }

    public void setNo_surat(String no_surat) {
        this.no_surat = no_surat;
    }


    public String getTanggal_surat() {
        return tanggal_surat;
    }

    public void setTanggal_surat(String tanggal_surat) {
        this.tanggal_surat = tanggal_surat;
    }

    public String getPerihal() {
        return perihal;
    }

    public void setPerihal(String perihal) {
        this.perihal = perihal;
    }

    public String getPerihalSubString() {
        return (perihal.length() > 120 ? perihal.substring(0, 120)+"..." : perihal);
    }

    public String getJenis_surat() {
        return jenis_surat;
    }

    public void setJenis_surat(String jenis_surat) {
        this.jenis_surat = jenis_surat;
    }


    public String getKet_lain() {
        return ket_lain;
    }

    public void setKet_lain(String ket_lain) {
        this.ket_lain = ket_lain;
    }

    public String getFile_surat() {
        return file_surat;
    }

    public void setFile_surat(String file_surat) {
        this.file_surat = file_surat;
    }

    public String getKepada() {
        return kepada;
    }

    public void setKepada(String kepada) {
        this.kepada = kepada;
    }

    @Override
    public Map<String, String> toMap() {
        Map map = super.toMap();

        map.remove("is_alowed");
        map.remove("perihalSubString");
        return map;
    }
}
