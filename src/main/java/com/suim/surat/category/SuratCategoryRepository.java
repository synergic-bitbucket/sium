package com.suim.surat.category;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Service;

@RepositoryRestResource
@Service("SuratCategoryRepository")
public interface SuratCategoryRepository extends CrudRepository<SuratCategory,Long>{

}
