package com.suim.surat.category;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.suim.api.domain.Jabatan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import com.suim.resource.DAOInterface;

@Service("SuratCategoryDAO")
public class SuratCategoryDAO implements DAOInterface<SuratCategory>{

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Override
	public SuratCategory getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SuratCategory> getByQuery(String query) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SuratCategory> getAll(int start, int limit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SuratCategory> getAll() {
		// TODO Auto-generated method stub
		return jdbcTemplate.query("select * from jenis_surat",new SuratCategoryRowMapper());
	}

	public List<SuratCategory> getAllSuratKeluarCategories() {
		// TODO Auto-generated method stub
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate.getDataSource());

		return jdbcTemplate.query("select * from jenis_surat_keluar",new SuratCategoryRowMapper());
	}

	@Override
	public boolean update(int id, SuratCategory data) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean create(SuratCategory data) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	class SuratCategoryRowMapper implements RowMapper<SuratCategory>{

		@Override
		public SuratCategory mapRow(ResultSet rs, int rowNum) throws SQLException {
			// TODO Auto-generated method stub
			SuratCategory suratCategory = new SuratCategory();
			suratCategory.setKode_surat(rs.getString("kode_surat") );
			suratCategory.setNama_jenis( rs.getString("nama_jenis") );
			return suratCategory;
		}
		
	}

    public List<Jabatan> getAllJabatan(){
        return jdbcTemplate.query("select * from pers_jabatan",new JabatanTypeHeadMapper());
    }

    public List<Jabatan> getDistribusiSurat(int idSurat){
        return jdbcTemplate.query("select a.* from pers_jabatan a,penerima_disposisi b where a.id_jabatan=b.id_jabatan and b.id_surat_masuk="+idSurat,new JabatanTypeHeadMapper());
    }

    public boolean distribusikan(int id,List<Jabatan> jabatan){
        jdbcTemplate.update("delete from penerima_disposisi where id_surat_masuk="+id);
        boolean result = false;
        for(Jabatan j:jabatan){
            boolean status = (jdbcTemplate.update("insert into penerima_disposisi(id_surat_masuk,id_jabatan) values("+id+","+j.getId_jabatan()+")"))>0;
            result = result || status;
        }
        jdbcTemplate.update("update surat_masuk set step_surat=7 where id_surat="+id);
        return result;
    }

    class JabatanTypeHeadMapper implements RowMapper<Jabatan>{

        @Override
        public Jabatan mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Jabatan(rs.getLong("id_jabatan"),rs.getString("jabatan"));
        }

    }
}
