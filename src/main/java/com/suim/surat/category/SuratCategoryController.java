package com.suim.surat.category;

import java.util.List;

import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/api")
public class SuratCategoryController {
	
	@Autowired
	private SuratCategoryRepository repo;
	
	@RequestMapping( value = "/category",method = RequestMethod.GET )
	public @ResponseBody List<SuratCategory> getSurat(){
		List<SuratCategory> surat = Lists.newArrayList( repo.findAll() );
		return surat;
	}
}
