package com.suim.surat.category;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class SuratCategory {
	
	@Id
	private String kode_surat;
	private String nama_jenis;

	public String getKode_surat() {
		return kode_surat;
	}

	public void setKode_surat(String kode_surat) {
		this.kode_surat = kode_surat;
	}

	public String getNama_jenis() {
		return nama_jenis;
	}

	public void setNama_jenis(String nama_jenis) {
		this.nama_jenis = nama_jenis;
	}
	
	
}
