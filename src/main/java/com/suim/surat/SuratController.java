package com.suim.surat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.suim.api.domain.SocketData;
import com.suim.config.WebSocketHandler;
import com.suim.surat.category.SuratCategoryDAO;
import com.suim.user.Personil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@Controller
@RequestMapping("/surat")
public class SuratController {

    @Autowired
    SuratDAO suratDAO;

    @Autowired
    SuratCategoryDAO suratCategoryDAO;

    @Autowired
    WebSocketHandler webSocketHandler;

    @Autowired
    public SimpMessageSendingOperations messagingTemplate;


    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String createSurat(Surat surat, Model model) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        surat.setTanggal_surat(format.format(Calendar.getInstance().getTime()));
        format.applyPattern("yyyy-MM-dd HH:mm");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 16);
        calendar.set(Calendar.MINUTE, 0);
        surat.setBatas_waktu(format.format(calendar.getTime()));

        model.addAttribute("listCategory", suratCategoryDAO.getAll());
        return "surat/lumino-create";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createSuratHandler(@Valid Surat surat, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "surat/lumino-create";
        }

        System.out.println(surat.getTanggal_surat());

        surat.setStep_surat(SuratDAO.TERUSKA_WAKA);
        suratDAO.create(surat);
        try {
            String json = new ObjectMapper().writeValueAsString(surat);
            messagingTemplate.convertAndSend("/surat-masuk/jabatan/"+Personil.USER_SRIPIM,json);
            messagingTemplate.convertAndSend("/surat-masuk/jabatan/"+Personil.USER_WAKA,json);

            webSocketHandler.sendToJabatan(Personil.USER_KA,new SocketData(SocketData.TYPE_SURAT_MASUK,json));
            webSocketHandler.sendToJabatan(Personil.USER_WAKA,new SocketData(SocketData.TYPE_SURAT_MASUK,json));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return "redirect:/suratmasuk";
    }

    @RequestMapping(value = "/deletemasuk", method = RequestMethod.GET)
    public String deleteSuratMasuk(@RequestParam(required = true) int id) {
        suratDAO.deleteSuratMasuk(id);
        return "redirect:/suratmasuk";
    }

    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public String detailSurat(@RequestParam(defaultValue = "0", required = true) int id, Model model) {
        Surat surat = suratDAO.getByIdDetail(id);

    //        if(surat.getStep_surat()==7){
    //            System.out.println(surat.getStep_surat());
    //            System.out.println(Personil.getRoles());
    //            if( !(Personil.getRoles().equals(Personil.USER_KA) || Personil.getRoles().equals(Personil.USER_SRIPIM) || Personil.getRoles().equals(Personil.USER_WAKA) || suratDAO.checkDistribusi(surat.getId_surat())) )
    //                return "redirect:/disposisi";
    //        }else {
    //            if ((surat.getStep_surat() == 2 &&
    //                    (
    //                            Personil.getRoles().equals(Personil.USER_ADMIN) ||
    //                                    Personil.getRoles().equals(Personil.USER_WAKA) ||
    //                                    Personil.getRoles().equals(Personil.USER_SRIPIM))
    //            )
    //                    || (surat.getStep_surat() == 4 && (Personil.getRoles().equals(Personil.USER_SRIPIM) ||
    //                    Personil.getRoles().equals(Personil.USER_KA)))) ;
    //            else
    //                return "redirect:/suratmasuk";
    //            if (surat.getStep_surat() >= 3 && surat.getStep_surat() != 4)
    //                return "redirect:/disposisi";
    //        }

        model.addAttribute("suratDetails", surat);
        System.out.println(surat.getBatas_waktu());
        String roles = Personil.getRoles();
        model.addAttribute("allowed",(surat.getStep_surat() < 3 && roles.equals(Personil.USER_ADMIN)  ) || roles.equals(Personil.USER_SRIPIM) );
        return "surat/lumino-detail";
    }


    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String updateSurat(@RequestParam(required = true) int id, Model model) {

        Surat surat = suratDAO.getById(id);

        if((surat.getStep_surat() == 2 && Personil.getRoles().equals(Personil.USER_ADMIN)) || Personil.getRoles().equals(Personil.USER_SRIPIM) );
        else
            return "redirect:/suratmasuk";
        if(surat.getStep_surat() >= 3 && surat.getStep_surat()!=4)
            return "redirect:/disposisi";


        System.out.println("ID_SURAT : " + surat.getId_surat());

        if (surat.getBatas_waktu() == null || surat.getBatas_waktu().equals("")) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            surat.setTanggal_surat(format.format(Calendar.getInstance().getTime()));
            format.applyPattern("yyyy-MM-dd HH:mm");
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, 16);
            calendar.set(Calendar.MINUTE, 0);
            surat.setBatas_waktu(format.format(calendar.getTime()));
        }
        System.out.println("STEP_SURAT : "+surat.getStep_surat());
        model.addAttribute("surat", surat);
        model.addAttribute("listCategory", suratCategoryDAO.getAll());

        return "surat/lumino-create";
    }


    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String updateSuratHandler(@Valid Surat surat, BindingResult bindingResult) {
        System.out.println("STEP_SURAT : "+surat.getStep_surat());
        if (bindingResult.hasErrors()) {
            return "surat/edit?id=" + surat.getId_surat();
        }

        System.out.println(suratDAO.update(surat.getId_surat(), surat) ? "update berhasil" : "update gagal");

        return "redirect:/suratmasuk";
    }

    @RequestMapping(value = "/disposisi", method = RequestMethod.POST)
    public String disposisiSuratMasuk(@RequestParam int id,@Valid Surat surat, BindingResult bindingResult) {

        System.out.println(surat.getDisposisi());
        System.out.println(id);
        surat.setId_surat(id);
        if (bindingResult.hasErrors() && !suratDAO.disposisiSuratMasuk(id,surat.getDisposisi())) {
            return "surat/edit?id=" + surat.getId_surat();
        }
        String json = null;
        try {
            json = new ObjectMapper().writeValueAsString(surat);
            messagingTemplate.convertAndSend("/surat-masuk/jabatan/"+Personil.USER_SRIPIM,json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "redirect:/suratmasuk";
    }

    @RequestMapping(value = "/teruskan", method = RequestMethod.POST)
    public String teruskanKeKA(@RequestParam(required = true) int id) {
        suratDAO.teruskan(id);

        String json = null;
        try {
            json = new ObjectMapper().writeValueAsString(suratDAO.getById(id));
            messagingTemplate.convertAndSend("/surat-masuk/jabatan/"+Personil.USER_SRIPIM,json);
            messagingTemplate.convertAndSend("/surat-masuk/jabatan/"+Personil.USER_KA,json);

            webSocketHandler.sendToJabatan(Personil.USER_KA,new SocketData(SocketData.TYPE_SURAT_MASUK,json));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return "redirect:/suratmasuk";
    }

    @RequestMapping(value = "/editkeluar", method = RequestMethod.GET)
    public String updateSuratKeluar(@RequestParam(required = true) int id, Model model) {

        SuratKeluar surat = suratDAO.getSuratKeluarById(id);

        System.out.println("ID_SURAT : " + surat.getId_surat());

        model.addAttribute("surat", surat);
        model.addAttribute("listCategory", suratCategoryDAO.getAllSuratKeluarCategories());

        return "surat/lumino-create_surat_keluar";
    }


    @RequestMapping(value = "/editkeluar", method = RequestMethod.POST)
    public String updateSuratKeluarHandler(@Valid SuratKeluar surat, BindingResult bindingResult) {

        if (bindingResult.hasErrors() || !suratDAO.updateSuratKeluar(surat)) {
            System.out.println("update gagal");
            return "surat/edit?id=" + surat.getId_surat();
        }

        System.out.println("update berhasil");

        return "redirect:/suratkeluar";
    }

    @RequestMapping(value = "/createkeluar", method = RequestMethod.GET)
    public String createSuratKeluar(SuratKeluar surat, Model model) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        surat.setTanggal_surat(format.format(Calendar.getInstance().getTime()));

        model.addAttribute("surat",surat);
        model.addAttribute("listCategory", suratCategoryDAO.getAllSuratKeluarCategories());
        return "surat/lumino-create_surat_keluar";
    }

    //TODO DAO INSERT SURAT KELUAR
    @RequestMapping(value = "/createkeluar", method = RequestMethod.POST)
    public String createSuratKeluarHandler(@Valid SuratKeluar surat, BindingResult bindingResult) {

        if (bindingResult.hasErrors() || !suratDAO.insertSuratKeluar(surat)) {
            System.out.println("gagal create surat keluar");
            return "surat/lumino-create_surat_keluar";
        }
        System.out.println("berhasil create surat keluar");

        return "redirect:/suratkeluar";
    }

    @RequestMapping(value = "/detailkeluar", method = RequestMethod.GET)
    public String detailSuratKeluar(@RequestParam(defaultValue = "0", required = true) int id, Model model) {
        SuratKeluar surat = suratDAO.getSuratKeluarById(id);
        model.addAttribute("suratDetails", surat);
        return "surat/lumino-detail_surat_keluar";
    }

    @RequestMapping(value = "/deletekeluar", method = RequestMethod.GET)
    public String deleteSuratKeluar(@RequestParam(required = true) int id) {
        suratDAO.deleteSuratKeluar(id);
        return "redirect:/suratkeluar";
    }

    @RequestMapping(value = "/buatdisposisi", method = RequestMethod.GET)
    public String disposisiUpload(@RequestParam(required = true) int id, Model model) {
        Surat surat = suratDAO.getById(id);
        model.addAttribute("surat", surat);
        return "surat/lumino-disposisi_upload";
    }

    @RequestMapping(value = "/buatdisposisi", method = RequestMethod.POST)
    public String disposisiUploadPost(@ModelAttribute Surat surat, BindingResult bindingResult) {
//        Surat surat = suratDAO.getById(id_surat);

        if (bindingResult.hasErrors() || !suratDAO.updateDisposisiAjudan(surat)) {
            System.out.println("error binding : "+bindingResult.hasErrors());
            return "surat/lumino-disposisi_upload";
        }

        String json = null;
        try {
            json = new ObjectMapper().writeValueAsString(surat);
            messagingTemplate.convertAndSend("/surat-masuk/jabatan/"+Personil.USER_SRIPIM,json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return "redirect:/suratmasuk";
    }

}
