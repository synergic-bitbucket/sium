package com.suim.config;

import com.suim.user.PersonilDAO;
import com.suim.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by raizal.pregnanta on 18/11/2016.
 */
@Service
public class UserDetailService implements UserDetailsService {

    @Autowired
    PersonilDAO personilDAO;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        User user = personilDAO.userByUsername(s);

        if (user == null) {
            return null;
        }

        List<GrantedAuthority> auth = AuthorityUtils.commaSeparatedStringToAuthorityList(user.getRole());

        return new com.suim.config.UserDetails(user,auth);
    }
}
