package com.suim.config;

import com.suim.SuimApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

@Configuration
@EnableWebSocketMessageBroker
public class STOMPWebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer{

	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint("/notif").setAllowedOrigins("*").withSockJS();
	}

	@Override
	public void configureMessageBroker(MessageBrokerRegistry config) {
        config.setApplicationDestinationPrefixes("/test");
	}

    @Override
    public void configureClientOutboundChannel(ChannelRegistration registration) {
        super.configureClientOutboundChannel(registration);
    }

    @Override
    public void configureClientInboundChannel(ChannelRegistration registration) {
        registration.setInterceptors(new ChannelInterceptor() {
            @Override
            public Message<?> preSend(Message<?> message, MessageChannel messageChannel) {
                StompHeaderAccessor accessor = StompHeaderAccessor.wrap(message);
                StompCommand command = accessor.getCommand();
                return message;
            }

            @Override
            public void postSend(Message<?> message, MessageChannel messageChannel, boolean b) {
                StompHeaderAccessor sha = StompHeaderAccessor.wrap(message);

                switch(sha.getCommand()) {
                    case CONNECT:

                        break;
                    case CONNECTED:
                        SuimApplication.online++;
                        break;
                    case DISCONNECT:
                        SuimApplication.online--;
                        break;
                    default:
                        break;

                }
            }

            @Override
            public void afterSendCompletion(Message<?> message, MessageChannel messageChannel, boolean b, Exception e) {

            }

            @Override
            public boolean preReceive(MessageChannel messageChannel) {
                return false;
            }

            @Override
            public Message<?> postReceive(Message<?> message, MessageChannel messageChannel) {
                return null;
            }

            @Override
            public void afterReceiveCompletion(Message<?> message, MessageChannel messageChannel, Exception e) {

            }
        });
    }
}
