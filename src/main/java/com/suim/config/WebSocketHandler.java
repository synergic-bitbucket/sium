package com.suim.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.suim.SuimApplication;
import com.suim.api.domain.SocketData;
import com.suim.resource.Auth;
import com.suim.user.Personil;
import com.suim.user.PersonilDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class WebSocketHandler extends TextWebSocketHandler {

    @Autowired
    PersonilDAO personilDAO;

    Map<String,List<WebSocketSession>> clients = new HashMap<>();
    Map<WebSocketSession,String> clientGroup = new HashMap<>();

    @Override
    public void handleTransportError(WebSocketSession session, Throwable throwable) throws Exception {

    }

    public List<WebSocketSession> getByJabatan(String jabatan) {
        List<WebSocketSession> c = clients.get(jabatan);
        return c==null?new ArrayList<>():c;
    }

    public void sendToJabatan(String jabatan,SocketData data){
        String json = null;
        try {
            json = new ObjectMapper().writeValueAsString(data);
            for(WebSocketSession session : getByJabatan(jabatan)){
                session.sendMessage(new TextMessage(json));
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        SuimApplication.online--;
        System.out.println("DISCONNECTED : "+session.getId());
        String key = clientGroup.get(session);
        clientGroup.remove(session);
        List<WebSocketSession> clients = this.clients.get(key);
        if(clients!=null)
            clients.remove(session);
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        SuimApplication.online++;
        try {
            System.out.println(session.getRemoteAddress().getAddress()+" connected");
            System.out.println(session.getId());
            List<String> data = session.getHandshakeHeaders().get("token");
            if(data!=null){
                String token = data.get(0);
                System.out.println("TOKEN FOUND : "+token);
                Personil personil = personilDAO.getById(Integer.valueOf(Auth.getTokenData(token).getBody().getId()));

                if(clients.get(personil.getId_jabatan()+"")==null){
                    clients.put(personil.getId_jabatan()+"",new ArrayList<>());
                }
                List connected = clients.get(personil.getId_jabatan()+"");
                connected.add(session);
                clientGroup.put(session,personil.getId_jabatan()+"");
            }else{
                System.out.println("TOKEN NOT FOUND");
                session.close(CloseStatus.NOT_ACCEPTABLE);
            }
        }catch (Exception e){
            session.close(CloseStatus.NOT_ACCEPTABLE);
            e.printStackTrace();
        }
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage jsonTextMessage) throws Exception {

    }
}
