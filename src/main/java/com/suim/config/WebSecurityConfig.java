package com.suim.config;

import com.suim.user.Personil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // TODO Auto-generated method stub
        http.authorizeRequests()
                .antMatchers("/bootstrap/**").permitAll()
                .antMatchers("/bootstrap/css/**").permitAll()
                .antMatchers("/assets/**").permitAll()
                .antMatchers("/websocketDev/**").permitAll()
                .antMatchers("/notification/**").permitAll()
                .antMatchers("/doc/**").permitAll()
                .antMatchers("/webjars/**").permitAll()
                .antMatchers("/vendors/**").permitAll()
                .antMatchers("/suim-notification/**").permitAll()
                .antMatchers("sium-apps-notification/**").permitAll()
                .antMatchers("/api/**").permitAll()
//			.antMatchers("/surat/edit").access("hasAuthority("+Personil.USER_SRIPIM+")"
//					+ " and hasAuthority("+Personil.USER_ADMIN+")")
//			.antMatchers("/surat/edit/**").hasAnyAuthority(Personil.USER_SRIPIM)
                .antMatchers("/surat/create/**").hasAuthority(Personil.USER_ADMIN)
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .defaultSuccessUrl("/", true)
                .usernameParameter("username")
                .passwordParameter("password")
                .loginPage("/login")
                .permitAll()
                .and().logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login")
                .permitAll()
                .and()
                .csrf()
                .ignoringAntMatchers("/api/**",
                        "/notification/**",
                        "/sium-apps-notification/**")
                .and()
                .headers().frameOptions().sameOrigin();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(new BCryptPasswordEncoder());
        return authenticationProvider;
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
//        String query = "select\n" +
//                "a.username , a.password,\n" +
//                "COALESCE(b.nama,c.nama) nama,COALESCE(c.id_jabatan,\"admin\") role\n" +
//                "from user a\n" +
//                "left join admin b on\n" +
//                "(b.id = a.id_pemilik and a.jenis_pemilik=\"admin\")\n" +
//                "left join personil c ON\n" +
//                "(c.id_personil=a.id_pemilik and a.jenis_pemilik=\"personil\" and c.statusdinas=1)\n" +
//                "where a.username=?";
//        auth.jdbcAuthentication()
//                .dataSource(dataSource)
//                .passwordEncoder(new BCryptPasswordEncoder())
//                .usersByUsernameQuery(query)
//                .authoritiesByUsernameQuery(query);
////                .usersByUsernameQuery("select nrp as username,nrp as password,statusdinas,nama from personil where nrp = ?")
////                .authoritiesByUsernameQuery("select nrp as username,id_jabatan as role from personil where nrp = ? and statusdinas=1");
    }

}
