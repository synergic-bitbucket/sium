package com.suim.datatables;

import com.suim.surat.Surat;
import com.suim.user.Personil;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by raizal.pregnanta on 10/12/2016.
 */
public class SuratMasuk {

    @Setter
    @Getter
    private String no_agenda;
    @Setter
    @Getter
    private String no_surat;
    @Setter
    @Getter
    private String jenis_surat;
    @Setter
    @Getter
    private String dari;
    @Setter
    @Getter
    private String tanggal_surat;
    @Setter
    @Getter
    private String datetime_diterima;
    @Setter
    @Getter
    private String perihal;

    @Setter
    @Getter
    private String status;

    @Getter @Setter private String aksi;

    @Setter
    @Getter
    private int id_surat;
    @Setter
    @Getter
    private int step;

    public SuratMasuk(Surat surat) {
        dari = surat.getDari();
        no_agenda = surat.getNo_agenda();
        no_surat = surat.getNo_surat();
        jenis_surat = surat.getJenis_surat();
        tanggal_surat = surat.getTanggal_surat();
        datetime_diterima = surat.getDatetime_diterima();
        perihal = surat.getPerihal();
        switch (surat.getStep_surat()) {
            case 2:
                status = "Diteruskan ke Waka";
                break;
            case 3:
                status = "Didisposisi Waka";
                break;
            case 4:
                status = "Diteruskan ke KA";
                break;
            case 5:
                status = "Didisposisi KA";
                break;
            case 6:
                status = "Didisposisi melalui Ajudan";
                break;
            case 7:
                status = "Terdistribusi";
                break;
        }
        id_surat = surat.getId_surat();
        step = surat.getStep_surat();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<a href=\"#\" onclick=\"detail("+id_surat+")\" class=\"btn btn-small btn-block btn-primary\">Buka</a>");
        if(Personil.getRoles().equals(Personil.USER_SRIPIM)) {
            switch (step) {
                case 3:
                case 5:
                case 6:
                case 7:
                    stringBuilder.append("<a onclick=\"requestDetailDistribusi(" + id_surat + ")\"\nclass=\"btn btn-small btn-block btn-primary\">Distribusikan</a>");
            }
        }
        aksi = stringBuilder.toString();
    }
}
