package com.suim.datatables;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.suim.utils.Object;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SuratKeluar{
    @Setter @Getter private  String  no_surat;
    @Setter @Getter private  String kepada;
    @Setter @Getter private  String  tanggal_surat;
    @Setter @Getter private  String  perihal;
    @Setter @Getter private  String  jenis_surat;
    @Getter @Setter private String aksi;

    @Setter @Getter private  String ket_lain;
    @Setter @Getter private  int id_surat;
    @Setter @Getter private  String file_surat;

    public SuratKeluar(com.suim.surat.SuratKeluar surat) {
        no_surat = surat.getNo_surat();
        kepada = surat.getKepada();
        tanggal_surat = surat.getTanggal_surat();
        perihal = surat.getPerihal().length()>=45?surat.getPerihal().substring(0,45)+(surat.getPerihal().length()>45?"...":"") : surat.getPerihal();
        jenis_surat = surat.getJenis_surat();
        ket_lain = surat.getKet_lain();
        id_surat = surat.getId_surat();
        file_surat = surat.getFile_surat();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<a href=\"#\" onclick=\"detailSuratKeluar("+id_surat+")\" class=\"btn btn-small btn-block btn-primary\">Buka</a>");
        aksi = stringBuilder.toString();
    }
}
