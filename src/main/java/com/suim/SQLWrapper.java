package com.suim;
import java.util.*;

/**
 * Created by raizal.pregnanta on 31/10/2016.
 */
public class SQLWrapper {

    private String query = "";
    private List<String> where = new ArrayList<>();
    private static SQLWrapper instance = new SQLWrapper();

    public static SQLWrapper where(String where){
        instance.where.add(where);
        return instance;
    }

    public static SQLWrapper delete(String table){
        instance.query="delete from "+table+" ";
        return  instance;
    }

    public static SQLWrapper update(String table, Map<String,Object> data){
        instance.query = "update "+table+" set ";
        Iterator<String> dataSet =  data.keySet().iterator();
        while(dataSet.hasNext()){
            String field = dataSet.next();
            if(data.get(field)==null){
                instance.query += field+" ";
            }else {
                instance.query += field + "=:" + field + " ";
            }
            if(dataSet.hasNext()){
                instance.query+=",";
            }
        }
        return instance;
    }

    public static SQLWrapper insert(String table, Map<String,Object> data){
        instance.query = "insert into "+table+"(";
        String values = "(";
        Iterator<String> dataSet =  data.keySet().iterator();
        while(dataSet.hasNext()){
            String field = dataSet.next();
            instance.query+=field;
            values+=":"+field;
            if(dataSet.hasNext()){
                instance.query+=",";
                values+=",";
            }

        }
        values+=")"; instance.query+=") values"+values;
        return instance;
    }

    public static String getSQL(){
        String query = instance.query;
        boolean first = true;
        for(String w : instance.where){
            if(first){
                first = false;
                query+=" where ";
            }
            query+=w;
        }
        instance.query = "";
        instance.where.clear();
        return query;
    }

}
