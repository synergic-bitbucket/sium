package com.suim.user;

import com.suim.config.UserDetailService;
import com.suim.config.UserDetails;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;

public class Personil {

    public final static String USER_KA = "1";
    public final static String USER_WAKA = "2";
    public final static String USER_ADMIN = "admin";
    public final static String USER_SRIPIM = "6";

    private int id_personil, nrp,
            id_agama, id_dikum, statusdinas,
            id_jabatan, kec, pangkatpertama;
    private String nama, tmptlahir, tgllahir,
            statuskawin, no_hp, alamat, no_rek,
            npwp, kelamin, deskel, kesatuan, tmtpolri;

    public static UserDetails getUserDetails(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object myUser = (auth != null) ? auth.getPrincipal() :  null;

        if (myUser instanceof UserDetails) {
            return (UserDetails) myUser;
        }
        return null;
    }

    public static String getRoles() {
        if(getUserDetails()==null || getUserDetails().getUser() == null || getUserDetails().getUser().getRole()==null)
            return "";

        return getUserDetails().getUser().getRole();
    }

    public static String getName() {
        if(getUserDetails()==null || getUserDetails().getUser() == null || getUserDetails().getUser().getRole()==null)
            return "";

        return getUserDetails().getUser().getPersonil().getNama();
    }

    public int getId_personil() {
        return id_personil;
    }

    public void setId_personil(int id_personil) {
        this.id_personil = id_personil;
    }

    public int getId_pangkat() {
        return id_jabatan;
    }

    public void setId_pangkat(int id_jabatan) {
        this.id_jabatan = id_jabatan;
    }

    public int getNrp() {
        return nrp;
    }

    public void setNrp(int nrp) {
        this.nrp = nrp;
    }

    public int getId_agama() {
        return id_agama;
    }

    public void setId_agama(int id_agama) {
        this.id_agama = id_agama;
    }

    public int getId_dikum() {
        return id_dikum;
    }

    public void setId_dikum(int id_dikum) {
        this.id_dikum = id_dikum;
    }

    public int getStatusdinas() {
        return statusdinas;
    }

    public void setStatusdinas(int statusdinas) {
        this.statusdinas = statusdinas;
    }

    public int getId_jabatan() {
        return id_jabatan;
    }

    public void setId_jabatan(int id_jabatan) {
        this.id_jabatan = id_jabatan;
        getPermission();
    }

    public int getKec() {
        return kec;
    }

    public void setKec(int kec) {
        this.kec = kec;
    }

    public int getPangkatpertama() {
        return pangkatpertama;
    }

    public void setPangkatpertama(int pangkatpertama) {
        this.pangkatpertama = pangkatpertama;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTmptlahir() {
        return tmptlahir;
    }

    public void setTmptlahir(String tmptlahir) {
        this.tmptlahir = tmptlahir;
    }

    public String getTgllahir() {
        return tgllahir;
    }

    public void setTgllahir(String tgllahir) {
        this.tgllahir = tgllahir;
    }

    public String getStatuskawin() {
        return statuskawin;
    }

    public void setStatuskawin(String statuskawin) {
        this.statuskawin = statuskawin;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNo_rek() {
        return no_rek;
    }

    public void setNo_rek(String no_rek) {
        this.no_rek = no_rek;
    }

    public String getNpwp() {
        return npwp;
    }

    public void setNpwp(String npwp) {
        this.npwp = npwp;
    }

    public String getKelamin() {
        return kelamin;
    }

    public void setKelamin(String kelamin) {
        this.kelamin = kelamin;
    }

    public String getDeskel() {
        return deskel;
    }

    public void setDeskel(String deskel) {
        this.deskel = deskel;
    }

    public String getKesatuan() {
        return kesatuan;
    }

    public void setKesatuan(String kesatuan) {
        this.kesatuan = kesatuan;
    }

    public String getTmtpolri() {
        return tmtpolri;
    }

    public void setTmtpolri(String tmtpolri) {
        this.tmtpolri = tmtpolri;
    }

    public String getPermission() {
        switch (id_jabatan + "") {
            case USER_SRIPIM:
                return "disposisi-tulis,disposisi-teruskan,suratmasuk-baca";
            case USER_KA:
                return "disposisi-tulis,suratmasuk-baca";
            case USER_WAKA:
                return "disposisi-tulis,disposisi-teruskan,suratmasuk-baca";
            case USER_ADMIN:
                return "disposisi-baca,suratmasuk-baca";
            default:
                return "disposisi-baca";
        }
    }

}
