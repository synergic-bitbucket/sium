package com.suim.user.pangkat;

public class Pangkat {
	private int id, golongan,urutan;
	private String pangkat,pangkat_lengkap;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getGolongan() {
		return golongan;
	}
	public void setGolongan(int golongan) {
		this.golongan = golongan;
	}
	public int getUrutan() {
		return urutan;
	}
	public void setUrutan(int urutan) {
		this.urutan = urutan;
	}
	public String getPangkat() {
		return pangkat;
	}
	public void setPangkat(String pangkat) {
		this.pangkat = pangkat;
	}
	public String getPangkat_lengkap() {
		return pangkat_lengkap;
	}
	public void setPangkat_lengkap(String pangkat_lengkap) {
		this.pangkat_lengkap = pangkat_lengkap;
	}
}
