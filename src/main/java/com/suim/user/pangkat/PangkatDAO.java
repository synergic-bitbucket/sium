package com.suim.user.pangkat;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.suim.resource.DAOInterface;

@Service("PangkatDAO")
public class PangkatDAO implements DAOInterface<Pangkat>{

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Override
	public Pangkat getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Pangkat> getByQuery(String query) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Pangkat> getAll(int start, int limit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Pangkat> getAll() {
		// TODO Auto-generated method stub
		return jdbcTemplate.query("select * from pers_pangkat", new PangkatRowMapper());
	}

	@Override
	public boolean update(int id, Pangkat data) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean create(Pangkat data) {
		// TODO Auto-generated method stub
		return false;
	}
	
	class PangkatRowMapper implements RowMapper<Pangkat>{

		@Override
		public Pangkat mapRow(ResultSet rs, int rowNum) throws SQLException {
			Pangkat pangkat = new Pangkat();
			pangkat.setGolongan( rs.getInt("golongan") );
			pangkat.setId( rs.getInt("id") );
			pangkat.setPangkat( rs.getString("pangkat") );
			pangkat.setPangkat_lengkap( rs.getString("pangkat_lengkap") );
			pangkat.setUrutan( rs.getInt("urutan") );
			return pangkat;
		}
		
	}
}
