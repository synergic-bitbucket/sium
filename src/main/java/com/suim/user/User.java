package com.suim.user;

/**
 * Created by raizal.pregnanta on 18/11/2016.
 */
public class User {

    private String username,password,nama,role;
    private Personil personil;

    public User() {
    }

    public User(String username, String password, String nama, String role, Personil personil) {
        this.username = username;
        this.password = password;
        this.nama = nama;
        this.role = role;
    }

    public Personil getPersonil() {
        return personil;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setPersonil(Personil personil) {
        this.personil = personil;
    }

}