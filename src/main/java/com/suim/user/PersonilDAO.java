package com.suim.user;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.suim.resource.DAOInterface;

@Service("UserDAO")
public class PersonilDAO implements DAOInterface<Personil>{
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public Personil getById(int id) {
		String sql  = "select * from personil where id_personil = ?";

        Personil personil = (Personil) jdbcTemplate.queryForObject(sql,
				new Object[]{ id },new PersonilRowMapper());
		return personil;
	}
	
	public Personil getById(String username,String password) {
		String sql  = "select * from personil where nrp = ? and nrp = ? and statusdinas != 2";
		Personil personil = (Personil) jdbcTemplate.queryForObject(sql, 
				new Object[]{ username,password },new PersonilRowMapper());
		return personil;
	}

	@Override
	public List<Personil> getByQuery(String query) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean update(int id, Personil data) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean create(Personil data) {
		// TODO Auto-generated method stub
		return false;
	}

	class PersonilRowMapper implements RowMapper<Personil> {

		@Override
		public Personil mapRow(ResultSet rs, int rowNum) throws SQLException {
			Personil personil = new Personil();
            personil.setId_personil(rs.getInt("id_personil"));
			personil.setNama( rs.getString("nama") );
			personil.setNrp( rs.getInt("nrp") );		
			personil.setId_jabatan( rs.getInt("id_jabatan") );
			personil.setId_pangkat( rs.getInt("id_jabatan") );
			return personil;
		}
		
	}

	@Override
	public List<Personil> getAll(int start, int limit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Personil> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public User userByUsername(String s){
        String query = "select\n" +
                "a.username , a.password,\n" +
                "COALESCE(b.nama,c.nama) nama,a.id_pemilik,COALESCE(c.id_jabatan,\"admin\") role\n" +
                "from user a\n" +
                "left join admin b on\n" +
                "(b.id = a.id_pemilik and a.jenis_pemilik=\"admin\")\n" +
                "left join personil c ON\n" +
                "(c.id_personil=a.id_pemilik and a.jenis_pemilik=\"personil\" and c.statusdinas=1)\n" +
                "where a.username=?";

        return jdbcTemplate.queryForObject(query, new String[]{s}, new RowMapper<User>() {
            @Override
            public User mapRow(ResultSet resultSet, int i) throws SQLException {
                Personil personil = null;

                User user = new User(
                        resultSet.getString(resultSet.findColumn("username")),
                        resultSet.getString(resultSet.findColumn("password")),
                        resultSet.getString(resultSet.findColumn("nama")),
                        resultSet.getString(resultSet.findColumn("role")),
                        null
                );

                if(resultSet.getString(resultSet.findColumn("role")).equals(Personil.USER_ADMIN)){
                    personil = jdbcTemplate
                            .queryForObject(
                                    "select a.* from admin a,user b where b.username=? and b.id_pemilik=a.id",
                                    new String[]{user.getUsername()},
                                    new RowMapper<Personil>() {
                                        @Override
                                        public Personil mapRow(ResultSet resultSet, int i) throws SQLException {

                                            Personil p = new Personil();
                                            p.setNama(resultSet.getString(resultSet.findColumn("nama")));
                                            p.setId_jabatan(-1);
                                            return p;
                                        }
                                    }
                            );
                }else {
                    personil = getById(resultSet.getInt(resultSet.findColumn("id_pemilik")));
                }
                user.setPersonil(personil);
                return user;
            }
        });
    }

}
