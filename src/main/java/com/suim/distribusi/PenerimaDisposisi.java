package com.suim.distribusi;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Required;

public class PenerimaDisposisi {
	
	int id;
	
	@NotNull
	int id_surat_masuk, id_jabatan;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId_surat_masuk() {
		return id_surat_masuk;
	}

	public void setId_surat_masuk(int id_surat_masuk) {
		this.id_surat_masuk = id_surat_masuk;
	}

	public int getId_pangkat() {
		return id_jabatan;
	}

	public void setId_pangkat(int id_jabatan) {
		this.id_jabatan = id_jabatan;
	}
	
	
}
