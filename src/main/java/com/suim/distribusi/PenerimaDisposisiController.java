package com.suim.distribusi;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.suim.notification.NotificationHandler;
import com.suim.surat.Surat;
import com.suim.surat.SuratDAO;

@Controller
@RequestMapping("/penerimaDisposisi")
public class PenerimaDisposisiController {
	
	@Autowired
	PenerimaDisposisiDAO penerimaDisposisiDAO;
	
	@Autowired
	SuratDAO suratDAO;

	@Autowired
	private SimpMessagingTemplate brokerMessagingTemplate;
	
	@RequestMapping(value = "/create",method = RequestMethod.POST)
	public @ResponseBody String Create(@ModelAttribute @Valid PenerimaDisposisi penerimaDisposisi){
		try{
			if(penerimaDisposisiDAO.create(penerimaDisposisi) ){
				Surat surat = suratDAO.getById( penerimaDisposisi.getId_surat_masuk() );
				surat.setStep_surat(SuratDAO.TELAH_DIDISTRIBUSIKAN);
				suratDAO.update(surat.getId_surat(),surat);
				
				this.brokerMessagingTemplate.convertAndSend(NotificationHandler.PREFIX_MESSAGE
						+penerimaDisposisi.getId_pangkat()+NotificationHandler.MESSAGE_DISTRIBUSI,"Surat Telah di distribusikan");
				
				System.out.println(NotificationHandler.PREFIX_MESSAGE
						+penerimaDisposisi.getId_pangkat()+NotificationHandler.MESSAGE_DISTRIBUSI);
				
				return "Surat Telah berhasil di distribusikan";				
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return "Surat gagal di distribusikan";
	}
	
}
