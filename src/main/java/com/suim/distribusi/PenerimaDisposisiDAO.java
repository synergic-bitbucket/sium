package com.suim.distribusi;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.suim.resource.DAOInterface;

@Service("PenerimaPosisiDAO")
public class PenerimaDisposisiDAO implements DAOInterface<PenerimaDisposisi>{
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Override
	public PenerimaDisposisi getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<PenerimaDisposisi> getByQuery(String query) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<PenerimaDisposisi> getAll(int start, int limit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<PenerimaDisposisi> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean update(int id, PenerimaDisposisi data) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean create(PenerimaDisposisi data) {
		String query = "INSERT INTO penerima_disposisi(id_surat_masuk, id_jabatan) VALUES (?,?)";
		return jdbcTemplate.update(query, new Object[]{ data.getId_surat_masuk(),data.getId_pangkat() }) > 0;
	}
	
	
	class PenerimaDisposisiRowMapper implements RowMapper<PenerimaDisposisi>{

		@Override
		public PenerimaDisposisi mapRow(ResultSet rs, int rowNum) throws SQLException {
			PenerimaDisposisi penerimaDisposisi = new PenerimaDisposisi();
			penerimaDisposisi.setId_surat_masuk( rs.getInt("id_surat_masuk") );
			penerimaDisposisi.setId_pangkat( rs.getInt("id_jabatan") );
			return penerimaDisposisi;
		}
		
	}

}
