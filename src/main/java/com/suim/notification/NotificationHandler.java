package com.suim.notification;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import com.suim.user.Personil;

public class NotificationHandler {
	
	public static final String PREFIX_MESSAGE = "/notification/get-message/";
	public static final String MESSAGE_DISPOSISI = "/notification-disposisi";
	public static final String MESSAGE_SIAP_DISPOSISI = "/notification-ready-disposisi";
	public static final String MESSAGE_DISTRIBUSI = "/notification-distribusi";
	
	
	@Autowired
	 private SimpMessagingTemplate brokerMessagingTemplate;
		
	public static ResponseStep getStep(String idJabatan,boolean isForSripim){
		String msg = "";
		List<String> listIdJabatan = new ArrayList<String>();
		
		if( isForSripim && ( idJabatan.equals(Personil.USER_KA) || idJabatan.equals(Personil.USER_WAKA) ) ){
			msg = "Surat Telah di disposisi, mohon untuk segera di cek dan di distribusikan";
			listIdJabatan.add(Personil.USER_SRIPIM);
		}else if( !isForSripim ){		
			switch (idJabatan) {
				case Personil.USER_ADMIN:
					msg = "Surat Telah dibuat oleh ADMIN untuk di disposisi";
					listIdJabatan.add(Personil.USER_WAKA);
					listIdJabatan.add(Personil.USER_SRIPIM);			
					break;			
				case Personil.USER_WAKA:
					msg = "Surat diteruskan kepada KA untuk di disposisi";
					listIdJabatan.add(Personil.USER_KA);
					break;
				default:
					break;
			}
		}						
		return new NotificationHandler().new ResponseStep(listIdJabatan,msg);
	}
	
	public void sendNotifDistribusi(String id,String idPangkat){
		final String message = "Surat Telah di distribusikan";
		brokerMessagingTemplate.convertAndSend("/notification/get-message/"+idPangkat+"/notification-distribusi",message);
	}
	
	public class ResponseStep{
		public List<String> jabatan;
		public String msg;
		
		public ResponseStep(List<String> jabatan,String msg) {
			this.jabatan = jabatan;
			this.msg = msg;			
		}
	}
}
	
	
