package com.suim.notification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.suim.resource.Auth;
import com.suim.user.Personil;
import com.suim.user.PersonilDAO;

@Controller
@MessageMapping("/jabatan")
public class NotificationsController {
	
	@Autowired
	private PersonilDAO personilDAO;
	
	@Autowired
	private SimpMessagingTemplate brokerMessagingTemplate;
	
//	@MessageMapping("/{id}")
//	public String greeting(@DestinationVariable int id, SimpMessageHeaderAccessor headerAccessor) throws Exception {
//		String id = headerAccessor.getSessionId();
//        return message + " your id is : "+id;
////		return message;
//    }
	
	


	@RequestMapping( value = "/notification/send-message-disposisi",method = RequestMethod.POST )
	@ResponseStatus(value = HttpStatus.OK)
	public void send(String token,@RequestParam(defaultValue = "0",required = false )int isForSripim){
		try{
			NotificationHandler.ResponseStep responseStep = NotificationHandler.getStep(
						String.valueOf( personilDAO.getById(Integer.parseInt(Auth.getId(token))).getId_jabatan() ),
					(isForSripim >= 0));
			for( String jabatan : responseStep.jabatan ){
				this.brokerMessagingTemplate.convertAndSend(NotificationHandler.PREFIX_MESSAGE+jabatan+NotificationHandler.MESSAGE_DISPOSISI, responseStep.msg);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping( value = "/notification/send-message-distribusi",method = RequestMethod.POST )
	@ResponseStatus(value = HttpStatus.OK)
	public void send(String token,String idJabatan){

	}
}
