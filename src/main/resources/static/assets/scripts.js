$(function(){
	$('#teruskan_ka').click(function(){
		if( $(this).is(":checked") ){
			$('input[type="file"],input[type="text"],textarea').attr('disabled','disabled');
		}else{
			$('input,textarea').removeAttr('disabled');
		}
	});
	
	$('#upload').change(function(e){
		e.preventDefault();
		var data = new FormData();
		var target = $('#'+$(this).data('target'));				
		var uploadedFile = $(this).prop('files')[0];		
		data.append('fileUploadedBefore',target.val());
		data.append('file',uploadedFile);		
		data.append('_csrf',$('meta[name="_csrf"]').attr('content'));
		
		$.ajax({
			url : $(this).attr('href'),
			type : 'post',
			enctype: 'multipart/form-data',
			data : data,
			contentType: false,
	        processData: false,
	        cache : false,
			success : function(response){
				console.log(response);
			}
		})
	})
	
	$('.distribusi-tugas').click(function(){
		$('#distribusiAlert').modal("show");
		$('#distribusi_id_surat_masuk_field').val( $(this).data("id") );		
		
	})
	
	$('#logoutBtn').click(function(){
		$('#logout').submit();
	})
	
	
	$("#form-distribusi").submit(function(e){
		e.preventDefault();		
		if( confirm("Surat akan di distribusikan, Apakah anda yakin ? ") ){
			$.ajax({
				url : $(this).attr('action'),
				data : $(this).serialize(),
				type : 'post',
				success : function(response){
					alert(response);					
					$('#distribusiAlert').modal('hide');
					window.location.href="/disposisi";
				},
				error : function(){
					alert('Distribusi surat gagal, ulangi dengan refresh halaman / tekan F5');
				}
			})
		}		
	})
	
	$('.search-element li a').click(function(e){
		e.preventDefault();
		var id = $(this).data("id");
		var searchText = $('#searchText').val().trim();
		var link = $('#searchForm').attr('action')+"?";
		if( searchText.trim().length > 0 ){
			link += "searchQuery="+searchText+"&";
		}
		link +="searchBy="+id;
		window.location.href=link;
		
	})
	
})
